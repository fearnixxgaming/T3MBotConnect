<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Song
 *
 * @property int $id
 * @property string $title
 * @property string $uri
 * @property int $user_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Song whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Song whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Song whereUri($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Song whereUserId($value)
 * @mixin \Eloquent
 * @property string|null $external_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Song whereExternalId($value)
 */
class Song extends Model
{
    //
    public $timestamps = false;
}
