<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class PermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $required_perms = collect(array_values(array_except(func_get_args(), [0, 1])));
        $user = Auth::user();

        if (!$user->activated || $user->banned) {
            Auth::guard()->logout();
            return back()->withErrors(['error' => "You're not allowed to view this"]);
        }

        $missing_perms = $required_perms->filter(function ($perm) use (&$user) {
            return !$user->hasPermission($perm);
        });

        if ($missing_perms->isNotEmpty()) {
            return back()->withErrors(['error' => "You're not allowed to view this"]);
        }

        return $next($request);
    }
}
