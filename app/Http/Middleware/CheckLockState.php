<?php

namespace App\Http\Middleware;

use App\MusicBot;
use App\MusicBotLock;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckLockState
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::user()->isAdmin()) {
            if ($request->has('mbot_id') || $request->route()->hasParameter('mbot_id')) {
                $mbot_id = $request->get('mbot_id');
                $mbots = MusicBot::whereId($mbot_id)->get();
                if ($mbots->count() > 0) {
                    $mbot = $mbots->get(0);
                    $lockstate = $mbot->getLockState();
                    switch ($lockstate) {
                        case MusicBotLock::STATES['LOCKED']:
                            return back()->withErrors(['error' => 'Bot locked']);
                        case MusicBotLock::STATES['OFFLINE']:
                            return back()->withErrors(['error' => 'Bot offline']);
                    }
                }
            }
        }
        return $next($request);
    }
}
