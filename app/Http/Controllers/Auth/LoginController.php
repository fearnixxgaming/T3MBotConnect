<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
        authenticated as performAuthenticated;
        showLoginForm as showLogin;
        logout as performLogout;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = LoginController::REDIR_IF_LOGGED_IN;

    const REDIR_IF_LOGGED_IN = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        $this->showLogin();
        return view('login');
    }

    public function logout(Request $request)
    {
        $this->performLogout($request);
        return redirect($this->redirectTo);
    }

    public function authenticated(Request $request, $user)
    {
        if ($user->banned) {
            $this->guard()->logout();
            return back()->withErrors(['error' => "Your account has been suspended"]);
        }
        if (!$user->activated) {
            $this->guard()->logout();
            return back()->withErrors(['error' => "Your account is not activated"]);
        }
        return $this->performAuthenticated($request, $user);
    }
}
