<?php

namespace App\Http\Controllers;

use App\Group;
use App\GroupLink;
use App\Permission;
use App\PermissionLink;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class GroupController extends Controller
{
    public static function routes()
    {
        Route::get('/groups', function () {
            return view('groups.list')->with(['groups' => Group::all()]);
        })
            ->middleware(
                'auth',
                'permission:' . Permission::GROUPS_VIEW
            )
            ->name('group.list');


        Route::get('/group/{group_id}', function (int $group_id) {
            $group = Group::whereId($group_id)->get()->get(0);
            $perms = Permission::all();
            return view('groups.show')->with(compact('group', 'perms', 'perm_links'));
        })
            ->middleware(
                'auth',
                'permission:' . Permission::GROUPS_VIEW
            )
            ->name('group.show');

        Route::post('/group/edit/perms')
            ->middleware(
                'auth',
                'permission:' . Permission::GROUPS_EDIT
            )
            ->uses('GroupController@editGroupPerms')
            ->name('group.edit.perms');

        Route::get('/groups/new', function () {
            return view('groups.new');
        })
            ->middleware(
                'auth'
            )
            ->name('group.register');

        Route::post('/group/new')
            ->middleware(
                'auth',
                'permission:' . Permission::GROUPS_EDIT
            )
            ->uses('GroupController@createGroup')
            ->name('group.new');

        Route::post('/groups/delete')
            ->middleware(
                'auth',
                'permission:' . Permission::GROUPS_EDIT
            )
            ->uses('GroupController@deleteGroup')
            ->name('group.delete');
    }

    public function editGroupPerms(Request $request)
    {
        $values = $request->all();
        $group = Group::whereId(intval($request->get('group_id')))->get()->get(0);
        $perm_links = PermissionLink::whereGroupId($group->id)->get();
        $update_count = 0;
        foreach ($perm_links as $link) {
            $name = 'perm-' . $link->perm_id;
            if (!array_key_exists($name, $values) || $values[$name] !== 'on') {
                $link->delete();
                $update_count++;
            }
        }
        foreach ($values as $key => $value) {
            if (strpos($key, 'perm-') === 0 && $value === 'on') {
                $perm_id = intval(substr($key, 5));
                if (PermissionLink::whereGroupId($group->id)->wherePermId($perm_id)->get()->count() === 0) {
                    $link = new PermissionLink();
                    $link->group_id = $group->id;
                    $link->perm_id = $perm_id;
                    $link->save();
                    $update_count++;
                }
            }
        }
        $errors = [];
        $errors['update_count'] = $update_count;
        return back()->withErrors($errors);
    }

    public function deleteGroup(Request $request)
    {
        $group = Group::whereId(intval($request->get('group_id')));
        $group->delete();
        return back();
    }

    public function createGroup(Request $request)
    {
        $name = $request->get('name');
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:4'
        ]);
        try {
            $validator->validate();
        } catch (ValidationException $exception) {
            return back()->withInput()->withErrors($validator->errors());
        }
        $group = Group::create([
            "display_name" => $name,
            "default" => false
        ]);
        $group->save();
        return redirect(route('group.show', $group->id));
    }
}
