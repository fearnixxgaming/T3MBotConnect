<?php

namespace App\Http\Controllers;

use App\Permission;
use App\TS3Channel;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Route;

class TS3Controller extends Controller
{
    public static function routes()
    {
        Route::get('/ts3/channels')
            ->middleware(
                'auth',
                'permission:' . Permission::BOTS_CONTROL
            )
            ->uses('TS3Controller@showChannels')
            ->name('channel.view');
    }

    public function showChannels(Request $request)
    {
        $view = view('forms.channel.viewChannelAJAX');
        $channels = TS3Channel::all();

        foreach ($channels as $channel) {
            $channel->name = $channel->getProperty('channel_name');
            $channel->order = intval($channel->getProperty('channel_order'));

            if ($channel->channel_parent_id !== 0) {
                $parent = $channels->filter(function ($theChannel) use ($channel) {
                    return $theChannel->channel_id === $channel->channel_parent_id;
                })->first();
                $parent->addChild($channel);
            }
        }

        $roots = $channels->filter(function ($c) {
            return $c->channel_parent_id === 0;
        });
        $counter = 0;
        self::incrementalSortChannels($roots, 0, $counter);

        $channels->keyBy('incr_order');
        $sortedChannels = [];
        for ($i = 0; $i < $channels->count(); $i++) {
            $sortedChannels[] = $channels->filter(function ($c) use ($i) {
                return $c->incr_order === $i;
            })->first();
        }

        return $view->with(['channels' => $sortedChannels])->render();
    }

    public static function incrementalSortChannels(Collection $collection, int $depth, int &$counter)
    {
        $lastID = 0;
        $internalCounter = 0;
        while ($internalCounter < $collection->count()) {
            foreach ($collection as $channel) {
                if ($channel->order === $lastID) {
                    $channel->incr_order = $counter++;
                    $channel->depth = $depth;
                    $lastID = $channel->channel_id;
                    $internalCounter++;
                    self::incrementalSortChannels($channel->getChildren(), $depth + 1, $counter);
                }
            }
        }
    }
}
