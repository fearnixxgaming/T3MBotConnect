<?php

namespace App\Http\Controllers;

use App\MusicBot;
use App\Permission;
use App\Playlist;
use App\PlaylistSongLink;
use App\Song;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class SongController extends Controller
{
    //
    public static function routes()
    {
        Route::get('/songs', function () {
            $songs = Song::all();
            $playlists = [];
            $playlists['own'] = Playlist::whereUserId(Auth::user()->id)->get();
            return view('listings.songs')->with(compact('songs', 'playlists'));
        })
            ->middleware(
                'auth',
                'permission:' . Permission::SONG_VIEW
            )
            ->name('song.list');

        Route::post('/songs/new')
            ->middleware(
                'auth',
                'permission:' . Permission::SONG_REGISTER
            )
            ->uses('SongController@registerSong')
            ->name('song.register');

        Route::post('/songs/delete')
            ->middleware(
                'auth',
                'permission:' . Permission::SONG_REGISTER
            )
            ->uses('SongController@deleteSong')
            ->name('song.delete');

        Route::post('/songs/play')
            ->middleware(
                'auth',
                'permission:' . Permission::BOTS_CONTROL,
                'lockstate'
            )
            ->uses('SongController@playSong')
            ->name('song.play');

        Route::post('/ajax/songInPlaylist')
            ->middleware(
                'auth',
                'permission:' . Permission::PLAYLIST_VIEW
            )
            ->uses('SongController@ajaxSongInPlaylist')
            ->name('song.AJAX.songInPlaylist');

        Route::post('/songs/songInPlaylist')
            ->middleware(
                'auth',
                'permission:' . Permission::PLAYLIST_VIEW
            )
            ->uses('SongController@songInPlaylist')
            ->name('song.songInPlaylist');
    }

    public function registerSong(Request $request)
    {
        $validator = Validator::make($request->all(['title', 'uri']), [
            "title" => "required|string",
            "uri" => [
                "required",
                "regex:/^(https|http|file):\/\/.+/u",
                "unique:songs"
            ]
        ]);
        try {
            $validator->validate();
        } catch (ValidationException $exception) {
            return back()->withInput()->withErrors($validator->errors());
        }

        $song = new Song();
        $song->title = $request->get('title');
        $song->uri = $request->get('uri');
        $song->user_id = Auth::user()->id;
        $song->save();
        return back();
    }

    public function deleteSong(Request $request)
    {
        $song = Song::whereId($request->get('song_id'))->get()->first();
        if (!is_null($song)) {
            $song->delete();
        }
        return back();
    }

    public function playSong(Request $request)
    {
        $song_id = $request->get('song_id');
        $song = Song::whereId($song_id)->get()->first();
        if (is_null($song)) {
            return back()->withErrors(['error' => 'Song not found!']);
        }

        $musicBot = MusicBot::whereId($request->get('mbot_id'))->get()->first();
        if (is_null($musicBot)) {
            return back()->withErrors(['error' => 'MusicBot not found!']);
        }
        $musicBot->playURL($song->uri);
        return back();
    }

    public function ajaxSongInPlaylist(Request $request)
    {
        $song_id = $request->get('song_id');
        $song = Song::whereId($song_id)->get()->first();
        if (is_null($song)) {
            return back()->withErrors(['error' => 'That song was not found!']);
        }
        $playlists = [];
        $playlists['own'] = Playlist::whereUserId(Auth::user()->id)->get();
        $view = view('forms.playlist.songInPlaylistAJAX', compact('song', 'playlists'));
        return $view->render();
    }

    public function songInPlaylist(Request $request)
    {
        $song_id = $request->get('song_id');
        $song = Song::whereId($song_id)->get()->first();
        if (is_null($song)) {
            return back()->withErrors(['error' => 'That song was not found!']);
        }

        $errors = [];
        $values = $request->all();
        $inPlaylists = [];
        $newPlaylistsCount = 0;
        foreach ($values as $key => $value) {
            if (str_contains($key, 'playlist')) {
                $arr = explode('-', $key);
                $id = $arr[1];
                if ($id === "new") {
                    $name = $arr[2];
                    $playlist = Playlist::whereUserId(Auth::user()->id)->whereDisplayName($name)->get()->first();
                    if (!is_null($playlist)) {
                        $errors[] = "Playlist name occupied: " . $playlist->display_name;
                    } else if (!Auth::user()->hasPermission(\App\Permission::PLAYLIST_CREATE_PRIVATE)) {
                        $errors[] = "You're not allowed to create playlists!";
                    } else if ($newPlaylistsCount >= 2) {
                        $errors[] = "You're not allowed to create more than 2 playlists per request";
                    } else {
                        $newPlaylistsCount++;
                        $playlist = new Playlist();
                        $playlist->user_id = Auth::user()->id;
                        $playlist->display_name = $name;
                        $playlist->save();

                        $playlistLink = new PlaylistSongLink();
                        $playlistLink->playlist_id = $playlist->id;
                        $playlistLink->song_id = $song->id;
                        if (!$playlistLink->save()) {
                            $errors[] = 'Failed to add song to playlist: ' . $playlist->display_name;
                        }
                        $inPlaylists['pl-' . $playlist->id] = true;
                    }
                } else {
                    $playlist = Playlist::whereId($id)->get()->first();
                    if (is_null($playlist)) {
                        $errors[] = 'Playlist not found for ID: ' . $id;
                    } else if ($playlist->user_id !== Auth::user()->id) {
                        $errors[] = 'Access denied for playlist with ID: ' . $id;
                    } else if ($value === 'on' || $value === 'true') {
                        $playlistLink = PlaylistSongLink::wherePlaylistId($playlist->id)->whereSongId($song->id)->get()->first();
                        if (is_null($playlistLink)) {
                            $playlistLink = new PlaylistSongLink();
                            $playlistLink->playlist_id = $playlist->id;
                            $playlistLink->song_id = $song->id;
                            if (!$playlistLink->save()) {
                                $errors[] = 'Failed to add song to playlist: ' . $playlist->display_name;
                            }
                        }
                        $inPlaylists['pl-' . $playlist->id] = true;
                    }
                }
            }
        }

        $allPlaylists = Playlist::whereUserId(Auth::user()->id)->get();
        foreach ($allPlaylists as $playlist) {
            if (!array_key_exists('pl-' . $playlist->id, $inPlaylists)) {
                $link = PlaylistSongLink::whereSongId($song->id)->wherePlaylistId($playlist->id)->get()->first();
                if (!is_null($link)) {
                    $link->delete();
                }
            }
        }

        return back()->withErrors(['error' => $errors]);
    }
}
