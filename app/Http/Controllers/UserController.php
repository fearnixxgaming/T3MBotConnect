<?php

namespace App\Http\Controllers;

use App\Group;
use App\GroupLink;
use App\Permission;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    public static function routes()
    {
        Route::get('/users', function () {
            return view('users.list')->with(['users' => User::all()->sortBy('activated')]);
        })
            ->middleware(
                'auth',
                'permission:' . Permission::USERS_VIEW
            )
            ->name('user.list');

        Route::get('/user/{user_id}', function (int $user_id) {
            $user = User::whereId($user_id)->get()->first();
            $groups = Group::all();
            $links = GroupLink::whereUserId($user->id)->get();
            return view('users.show')->with(compact('user', 'groups', 'links'));
        })
            ->middleware(
                'auth',
                'permission:' . Permission::USERS_VIEW
            )
            ->name('user.show');

        Route::post('/user/edit/groups')
            ->middleware(
                'auth',
                'permission:' . Permission::USERS_ASSIGN
            )
            ->uses('UserController@editUserGroups')
            ->name('user.edit.groups');

        Route::post('/user/edit/activate')
            ->middleware(
                'auth',
                'permission:' . Permission::USERS_EDIT
            )
            ->uses('UserController@activateUser')
            ->name('user.activate');

        Route::post('/user/edit/ban')
            ->middleware(
                'auth',
                'permission:' . Permission::USERS_ASSIGN
            )
            ->uses('UserController@banUser')
            ->name('user.ban');

        Route::get('/users/new', function () {
            return view('users.new');
        })
            ->middleware(
                'auth',
                'permission:' . Permission::USERS_EDIT
            )
            ->name('user.register');

        Route::post('/user/new')
            ->middleware(
                'auth',
                'permission:' . Permission::USERS_EDIT
            )
            ->uses('UserController@createUser')
            ->name('user.new');

        Route::post('/user/delete')
            ->middleware(
                'auth',
                'permission:' . Permission::USERS_EDIT
            )
            ->uses('UserController@deleteUser')
            ->name('user.delete');
    }

    public function editUserGroups(Request $request)
    {
        $user_id = intval($request->get('user_id'));
        $user = User::whereId($user_id)->get()->first();
        if (is_null($user)) {
            return back()->withErrors(['error' => "This user doesn't exist"]);
        }

        $currentUserIsAdmin = Auth::user()->isAdmin();
        if ($user->isAdmin() && !$currentUserIsAdmin) {
            return back()->withErrors(['error' => "You're not allowed to assign this user!"]);
        }

        $errors = [];
        $errors['error'] = [];
        $errors['update_count'] = 0;
        $values = $request->all();
        $group_links = GroupLink::whereUserId($user_id)->get();

        foreach ($group_links as $index => $link) {
            $name = 'group-' . $link->group_id;
            if (!array_key_exists($name, $values) || $values[$name] !== 'on') {
                $link->delete();
                $errors['update_count']++;
            }
        }

        foreach ($values as $key => $value) {
            if (strpos($key, 'group-') === 0 && $value === 'on') {
                $group_id = intval(substr($key, 6));

                if (GroupLink::whereUserId($user_id)->whereGroupId($group_id)->get()->count() === 0) {
                    $group = Group::whereId($group_id)->get()->first();
                    if ($group->isAdmin() && !$currentUserIsAdmin) {
                        $errors['error'][] = "You're not allowed to add users to this group!";
                    } else {
                        $link = new GroupLink();
                        $link->user_id = $user_id;
                        $link->group_id = $group_id;
                        $link->save();
                        $errors['update_count']++;
                    }
                }
            }
        }
        return back()->withErrors($errors);
    }

    public function activateUser(Request $request)
    {
        $user = User::whereId($request->get('user_id'))->get()->first();
        if (is_null($user)) {
            return back()->withErrors(['error' => "That user doesn't exist"]);
        }
        $active = $request->get('active');
        if ($active === 'on' || $active === 'true') {
            $user->activated = true;
            $user->save();
        } else {
            $user->activated = false;
            $user->save();
        }
        return back();
    }

    public function banUser(Request $request)
    {
        $user = User::whereId($request->get('user_id'))->get()->first();
        if (is_null($user)) {
            return back()->withErrors(['error' => "That user doesn't exist"]);
        }
        $banned = $request->get('banned');
        if ($banned === 'on' || $banned === 'true') {
            $user->banned = true;
            $user->save();
        } else {
            $user->banned = false;
            $user->save();
        }
        return back();
    }

    public function deleteUser(Request $request)
    {
        $user = User::whereId($request->get('user_id'))->get()->first();
        if (!is_null($user)) {
            $user->delete();
        }
        return back();
    }

    public function createUser(Request $request)
    {
        $name = $request->get('name');
        $email = $request->get('email');
        $password = $request->get('password');

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);
        try {
            $validator->validate();
        } catch (ValidationException $exception) {
            return back()->withInput()->withErrors($validator->errors());
        }

        $user = User::create([
            "name" => $name,
            "email" => $email,
            "password" => Hash::make($password)
        ]);
        $user->save();
        return redirect(route('user.show', $user->id));
    }
}
