<?php

namespace App\Http\Controllers;

use App\LockShareRequest;
use App\MusicBot;
use App\MusicBotLock;
use App\Permission;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class MusicBotLockController extends Controller
{
    public static function routes()
    {
        Route::post('/musicbot/lock')
            ->middleware(
                'auth',
                'permission:' . Permission::BOTS_CONTROL
            )
            ->uses('MusicBotLockController@lockBot')
            ->name('mbot.lock');

        Route::post('/musicbot/relock')
            ->middleware(
                'auth',
                'permission:' . Permission::BOTS_CONTROL
            )
            ->uses('MusicBotLockController@relockBot')
            ->name('mbot.relock');

        Route::post('/musicbot/unlock')
            ->middleware(
                'auth',
                'permission:' . Permission::BOTS_CONTROL
            )
            ->uses('MusicBotLockController@unlockBot')
            ->name('mbot.unlock');

        Route::post('/musicbot/requestshare')
            ->middleware(
                'auth',
                'permission:' . Permission::BOTS_CONTROL
            )
            ->uses('MusicBotLockController@requestShareBot')
            ->name('mbot.requestlock');

        Route::post('/musicbot/sharelock')
            ->middleware(
                'auth',
                'permission:' . Permission::BOTS_CONTROL,
                'lockstate'
            )
            ->uses('MusicBotLockController@shareBot')
            ->name('mbot.sharelock');

        Route::post('/musicbot/unsharelock')
            ->middleware(
                'auth',
                'permission:' . Permission::BOTS_CONTROL,
                'lockstate'
            )
            ->uses('MusicBotLockController@unshareBot')
            ->name('mbot.unsharelock');
    }

    public function lockBot(Request $request)
    {
        $musicBot = MusicBot::whereId($request->get('mbot_id'))->get()->get(0);
        $lockstate = $musicBot->getLockState();
        $user_id = Auth::user()->id;

        if ($lockstate !== 0) {
            return back()->withErrors(['error' => 'Invalid lockstate']);
        }

        $locks = MusicBotLock::whereId($musicBot->id)->whereUserId($user_id)->get();
        if ($locks->count() > 0) {
            $lock = $locks->get(0);
        } else {
            $lock = new MusicBotLock();
        }
        $lock->user_id = $user_id;
        $lock->mbot_id = $musicBot->id;
        $lock->refreshed_on = Carbon::now(config('app.timezone'));
        $result = $lock->save();
        return back()->with(['result' => $result]);
    }

    public function relockBot(Request $request)
    {
        $mbot_id = $request->get('mbot_id');
        $musicBot = MusicBot::whereId($mbot_id)->get()->first();
        $lockstate = $musicBot->getLockState();
        $user_id = Auth::user()->id;

        if ($lockstate !== 1) {
            $errors = [];
            $errors['error'] = 'Invalid lockstate';
            return back()->withErrors($errors);
        }

        $lock = MusicBotLock::whereMbotId($mbot_id)->whereUserId($user_id)->get()->first();
        if (is_null($lock)) {
            return back()->withErrors(['error' => 'No lock to refresh!']);
        } else {
            $lock->refreshed_on = Carbon::now(config('app.timezone'));
            $result = $lock->save();
            return back()->with(['result' => $result]);
        }
    }

    public function unlockBot(Request $request)
    {
        $mbot_id = $request->get('mbot_id');
        $musicBot = MusicBot::whereId($mbot_id)->get()->get(0);
        $lockstate = $musicBot->getLockState();
        $user_id = Auth::user()->id;

        if ($lockstate !== 1) {
            return back()->withErrors(['error' => 'Invalid lockstate']);
        }

        $locks = MusicBotLock::whereMbotId($mbot_id)->whereUserId($user_id)->get();
        if ($locks->count() === 0) {
            return back()->withErrors(['error' => 'No lock to release!']);
        } else {
            $lock = $locks->get(0);
            $result = $lock->delete();
            return back()->with(['result' => $result]);
        }
    }

    public function requestShareBot(Request $request)
    {
        $mbot_id = $request->get('mbot_id');
        $musicBot = MusicBot::whereId($mbot_id)->get()->get(0);
        $lockstate = $musicBot->getLockState();
        $user_id = Auth::user()->id;

        // Can only request access to active locks not owned by the user
        if ($lockstate !== MusicBotLock::STATES['LOCKED']) {
            return back()->withErrors(['error' => "Invalid lockstate"]);
        }

        $lock_request = LockShareRequest::whereUserId($user_id)->get()->first();
        if (is_null($lock_request)) {
            $lock_request = new LockShareRequest();
            $lock_request->user_id = $user_id;
        }
        $lock_request->ignored = $lock_request->mbot_id === $mbot_id;
        $lock_request->mbot_id = $mbot_id;
        $lock_request->refreshed_on = Carbon::now(config('app.timezone'));
        if (!$lock_request->save()) {
            return back()->withErrors(['error' => 'Failed to save request']);
        }
        return back()->with(['result' => true]);
    }

    public function shareBot(Request $request)
    {
        $request_id = $request->get('share_id');
        $share_request = LockShareRequest::whereId($request_id)->get()->first();
        if (is_null($share_request)) {
            return back()->withErrors(['error' => 'Share request not found']);
        }

        $mbot_id = $share_request->mbot_id;
        $musicBot = MusicBot::whereId($mbot_id)->get()->get(0);
        $lockstate = $musicBot->getLockState();
        $user_id = Auth::user()->id;

        // Can only share own active locks
        if ($lockstate !== MusicBotLock::STATES['OWNED']) {
            return back()->withErrors(['error' => 'Invalid lockstate']);
        }

        $lock = MusicBotLock::whereMbotId($mbot_id)->whereUserId($share_request->user_id)->get()->first();
        if (is_null($lock)) {
            $lock = new MusicBotLock();
            $lock->mbot_id = $mbot_id;
            $lock->user_id = $share_request->user_id;
        }
        $lock->shared_via = MusicBotLock::whereMbotId($mbot_id)->whereUserId($user_id)->get()->first()->id;
        $lock->refreshed_on = Carbon::now(config('app.timezone'));
        if (!$lock->save()) {
            return back()->withErrors(['error' => 'Failed to save shared lock']);
        }
        $share_request->delete();
        return back()->with(['result' => true]);
    }

    public function unshareBot(Request $request)
    {
        $ownLock = MusicBotLock::whereUserId(Auth::user()->id)->whereNotShared()->whereMbotId($request->get('mbot_id'))->get()->first();
        if (is_null($ownLock)) {
            return back()->withErrors(['error' => "You don't own the parent lock!"]);
        }

        $otherLock = MusicBotLock::whereId($request->get('lock_id'))->whereSharedVia($ownLock->id)->get()->first();
        if (!is_null($otherLock)) {
            $otherLock->delete();
        }
        return back();
    }
}
