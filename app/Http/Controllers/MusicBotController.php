<?php

namespace App\Http\Controllers;

use App\DbSetting;
use App\LockShareRequest;
use App\MusicBot;
use App\MusicBotLock;
use App\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class MusicBotController extends Controller
{
    public static function routes()
    {
        Route::get('/musicbots', function () {
            $musicBotsPerPage = intval(DbSetting::whereIdent(DbSetting::PP_MUSICBOTS)->get()->first()->getValue());
            return view('mbots.list')->with(['musicbots' => MusicBot::paginate($musicBotsPerPage)]);
        })->middleware(
            'auth',
            'permission:' . Permission::BOTS_VIEW
        )->name('mbot.list');

        Route::get('/musicbot/{mbot_id}', function (int $mbot_id) {
            $musicBot = MusicBot::whereId($mbot_id)->get()->get(0);
            $state = $musicBot->getLockState();
            $info = [
                "playing" => $musicBot->getTrackName(),
                "length" => $musicBot->getTrackLength(),
                "volume" => $musicBot->getVolume()
            ];
            $share_requests = collect([]);
            $shares_active = collect([]);
            if ($state === MusicBotLock::STATES['OWNED']) {
                $share_requests = LockShareRequest::whereMbotId($mbot_id)->get();
                $shares_active = MusicBotLock::whereMbotId($mbot_id)->whereShared()->get();
            }
            return view('mbots.show')->with(compact('musicBot', 'state', 'info', 'share_requests', 'shares_active'));
        })->middleware(
            'auth',
            'permission:' . Permission::BOTS_VIEW
        )->name('mbot.show');

        Route::get('/musicbots/new', function () {
            return view('mbots.new');
        })->middleware(
            'auth',
            'permission:' . Permission::BOTS_EDIT
        )->name('mbot.register');

        Route::post('/musicbot/new')
            ->middleware(
                'auth',
                'permission:' . Permission::BOTS_EDIT
            )
            ->uses('MusicBotController@createBot')
            ->name('mbot.new');

        Route::post('/musicbot/delete')
            ->middleware(
                'auth',
                'permission:' . Permission::BOTS_EDIT
            )
            ->uses('MusicBotController@deleteBot')
            ->name('mbot.delete');

        Route::post('/musicbot/stop')
            ->middleware(
                'auth',
                'permission:' . Permission::BOTS_CONTROL,
                'lockstate'
            )
            ->uses('MusicBotController@stop')
            ->name('mbot.stop');

        Route::post('/musicbot/prev')
            ->middleware(
                'auth',
                'permission:' . Permission::BOTS_CONTROL,
                'lockstate'
            )
            ->uses('MusicBotController@previous')
            ->name('mbot.prev');

        Route::post('/musicbot/next')
            ->middleware(
                'auth',
                'permission:' . Permission::BOTS_CONTROL,
                'lockstate'
            )
            ->uses('MusicBotController@next')
            ->name('mbot.next');

        Route::post('/musicbot/play')
            ->middleware(
                'auth',
                'permission:' . Permission::BOTS_CONTROL,
                'lockstate'
            )
            ->uses('MusicBotController@play')
            ->name('mbot.play');

        Route::post('/musicbot/queue')
            ->middleware(
                'auth',
                'permission:' . Permission::BOTS_CONTROL,
                'lockstate'
            )
            ->uses('MusicBotController@queueSong')
            ->name('mbot.queuesong');

        Route::post('/musicbot/volume')
            ->middleware(
                'auth',
                'permission:' . Permission::BOTS_CONTROL,
                'lockstate'
            )
            ->uses('MusicBotController@setVolume')
            ->name('mbot.volume');

        Route::post('/musicbot/mute')
            ->middleware(
                'auth',
                'permission:' . Permission::BOTS_CONTROL,
                'lockstate'
            )
            ->uses('MusicBotController@mute')
            ->name('mbot.mute');

        Route::post('/musicbot/channel')
            ->middleware(
                'auth',
                'permission:' . Permission::BOTS_CONTROL,
                'lockstate'
            )
            ->uses('MusicBotController@sendChannel')
            ->name('mbot.channel');
    }

    public function createBot(Request $request)
    {
        $errors = [];
        $musicbot = new MusicBot();
        $musicbot->url = $request->get('url');
        $musicbot->port = $request->get('port');
        $musicbot->admin_pass = $request->get('adminpass');
        $musicbot->display_name = $request->get('displayname');
        $mode = $request->get('mode');

        $existing = MusicBot::whereUrl($musicbot->url)->wherePort($musicbot->port)->get();
        if ($existing->count() > 0) {
            $errors['duplicate'] = 'Bot already registered!';
            $errors['port'] = 'Taken';
            return back()->withInput()->withErrors($errors);
        }

        $testresult = $musicbot->ping();
        if ($testresult === false) {
            $errors['connection'] = 'Connection failed!';
            return back()->withInput()->withErrors($errors);
        }

        if ($mode === 'TEST') {
            return back()->withInput()->with(['test' => true]);
        } else if ($mode === 'CREATE') {
            $musicbot->save();
            return back()->with(['result' => true]);
        }
    }

    public function deleteBot(Request $request)
    {
        $mbot_id = intval($request->get('mbot_id'));
        $musicBot = MusicBot::whereId($mbot_id);
        if (!is_null($musicBot)) {
            $musicBot->delete();
        }
        return back();
    }

    public function stop(Request $request)
    {
        $musicBot = MusicBot::whereId($request->get('mbot_id'))->get()->first();
        $musicBot->stop();
        return back();
    }

    public function previous(Request $request)
    {
        $musicBot = MusicBot::whereId($request->get('mbot_id'))->get()->first();
        $musicBot->previous();
        return back();
    }

    public function next(Request $request)
    {
        $musicBot = MusicBot::whereId($request->get('mbot_id'))->get()->first();
        $musicBot->next();
        return back();
    }

    public function setVolume(Request $request)
    {
        $musicBot = MusicBot::whereId($request->get('mbot_id'))->get()->first();
        $musicBot->setVolume($request->get('volume'));
        return back();
    }

    public function mute(Request $request)
    {
        $musicBot = MusicBot::whereId($request->get('mbot_id'))->get()->first();
        $musicBot->mute();
        return back();
    }

    public function play(Request $request)
    {
        $musicBot = MusicBot::whereId($request->get('mbot_id'))->get()->first();
        if ($request->get('playtype') === 'URL') {
            $musicBot->playURL($request->get('url'));
            return back();
        } elseif ($request->get('playtype') === 'FILE') {
            return back()->withInput()->withErrors(['url' => 'Files not yet supported']);
        }
        return back()->withInput()->withErrors(['url' => 'Invalid input']);
    }

    public function queueSong(Request $request)
    {
        $musicBot = MusicBot::whereId($request->get('mbot_id'))->get()->first();
        if (is_null($musicBot)) {
            return back()->withErrors(['error' => 'Invalid musicbot ID']);
        }

        $musicBot->queueURL($request->get('song_uri'));
        return back();
    }

    public function sendChannel(Request $request)
    {
        $musicBot = MusicBot::whereId($request->get('mbot_id'))->get()->first();
        $channelId = $request->get('channel_id');
        $musicBot->sendChannel($channelId);
        return back();
    }
}
