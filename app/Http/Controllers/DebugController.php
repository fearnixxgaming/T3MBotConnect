<?php

namespace App\Http\Controllers;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class DebugController extends Controller
{
    /*  @var $customFields Collection */
    private static $customFields;

    public static function routes(): void
    {
        DebugController::$customFields = new Collection();

        Route::get('/debug/session/clear')
            ->middleware('auth')
            ->uses('DebugController@clearSession')
            ->name('debug.session.clear');
    }

    public static function registerVolatileSessionField(string $key): void
    {
        DebugController::$customFields->push($key);
    }

    public function clearSession()
    {
        $session = Auth::getSession();
        if (!is_null($session)) {
            foreach (DebugController::$customFields as $field) {
                $session->forget($field);
            }
        }
        return back();
    }
}
