<?php

namespace App\Http\Controllers;

use App\Permission;
use App\Playlist;
use App\PlaylistSongLink;
use App\Song;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class PlaylistController extends Controller
{
    public static function routes()
    {
        Route::get('/playlists')
            ->middleware(
                'auth',
                'permission:' . Permission::PLAYLIST_VIEW
            )
            ->uses('PlaylistController@viewPlaylists')
            ->name('playlist.list');

        Route::get('/ajax/playlists')
            ->middleware(
                'auth',
                'permission:' . Permission::PLAYLIST_VIEW
            )
            ->uses('PlaylistController@ajaxViewPlaylist')
            ->name('playlist.ajaxList');

        Route::post('/playlist/publish')
            ->middleware(
                'auth',
                'permission:' . Permission::PLAYLIST_CREATE_PUBLIC
            )
            ->uses('PlaylistController@publishPlaylist')
            ->name('playlist.publish');

        Route::post('/playlist/depublish')
            ->middleware(
                'auth',
                'permission:' . Permission::PLAYLIST_CREATE_PUBLIC
            )
            ->uses('PlaylistController@depublishPlaylist')
            ->name('playlist.depublish');

        Route::post('/ajax/playlist/view')
            ->middleware(
                'auth',
                'permission:' . Permission::PLAYLIST_VIEW
            )
            ->uses('PlaylistController@ajaxViewPlaylist')
            ->name('playlist.view');

        Route::post('/ajax/playlist/songDelete')
            ->middleware(
                'auth'
            )
            ->uses('PlaylistController@deleteSongFromPlaylist')
            ->name('playlist.songdelete');

        Route::post('/ajax/playlist/playlistDelete')
            ->middleware(
                'auth'
            )
            ->uses('PlaylistController@deletePlaylist')
            ->name('playlist.delete');

        Route::post('/ajax/playlist/play')
            ->middleware(
                'auth',
                'permission:' . Permission::BOTS_CONTROL
            )
            ->uses('PlaylistController@playlistQueueInfo')
            ->name('playlist.play');
    }

    public function viewPlaylists(Request $request)
    {
        $ajax = str_contains($request->getRequestUri(), 'ajax');
        $playlists = [];
        $playlists['own'] = Playlist::whereUserId(Auth::user()->id)->get();
        $playlists['public'] = Playlist::whereIsPublic(true)->get();
        $playlists['other'] = collect([]);
        if (Auth::user()->hasPermission(Permission::PLAYLIST_MODIFY_UNOWNED))
        {
            $playlists['other'] = Playlist::whereNotUserId(Auth::user()->id)->whereIsPublic(false)->get();
        }

        if ($ajax)
        {
            return view('listings.playlists-body')->with(compact('playlists'));
        } else
        {
            return view('listings.playlists')->with(compact('playlists'));
        }
    }

    public function ajaxViewPlaylist(Request $request)
    {
        $playlist = Playlist::whereId($request->get('playlist_id'))->get()->first();
        if (is_null($playlist)) {
            return back()->withErrors(['error' => 'Playlist not found.']);
        }
        $songLinks = PlaylistSongLink::wherePlaylistId($playlist->id)->get();
        $songs = [];
        foreach ($songLinks as $songLink) {
            $songs[] = Song::whereId($songLink->song_id)->get()->first();
        }
        $songs = collect($songs);

        $view = view('forms.playlist.viewPlaylistAJAX', compact('playlist', 'songs'));
        return $view->render();
    }

    public function deleteSongFromPlaylist(Request $request)
    {
        $playlist = Playlist::whereId($request->get('playlist_id'))->get()->first();
        if (is_null($playlist)) {
            return back()->withErrors(['error' => 'Playlist not found.']);
        }

        if ($playlist->user_id != Auth::user()->id
            && !Auth::user()->hasPermission(Permission::PLAYLIST_MODIFY_UNOWNED)) {
            return back()->withErrors(['error' => 'You\'re not allowed to edit this!']);
        }

        $songLink = PlaylistSongLink::whereSongId($request->get('song_id'))->wherePlaylistId($playlist->id)->get()->first();

        if (!is_null($songLink)) {
            $songLink->delete();
        }
        return back();
    }

    public function deletePlaylist(Request $request)
    {
        $playlist = Playlist::whereId($request->get('playlist_id'))->get()->first();
        if (!is_null($playlist)) {
            $playlist->delete();
        }
        return back();
    }

    public function publishPlaylist(Request $request)
    {
        $playlist = Playlist::whereId($request->get('playlist_id'))->get()->first();
        if (is_null($playlist)) {
            return back()->withErrors(['error' => "This playlist doesn't exist!"]);
        }

        if ($playlist->user_id !== Auth::user()->id && !Auth::user()->hasPermission(Permission::PLAYLIST_MODIFY_UNOWNED)) {
            return back()->withErrors(['error' => "You're not allowed to edit this playlist"]);
        }

        $playlist->is_public = 1;
        $playlist->save();
        return back();
    }

    public function depublishPlaylist(Request $request)
    {
        $playlist = Playlist::whereId($request->get('playlist_id'))->get()->first();
        if (is_null($playlist))
        {
            return back()->withErrors(['error' => "This playlist doesn't exist!"]);
        }

        if ($playlist->user_id !== Auth::user()->id && !Auth::user()->hasPermission(Permission::PLAYLIST_MODIFY_UNOWNED))
        {
            return back()->withErrors(['error' => "You're not allowed to edit this playlist"]);
        }

        $playlist->is_public = 0;
        $playlist->save();
        return back();
    }

    public function playlistQueueInfo(Request $request)
    {
        $playlistLinks = PlaylistSongLink::wherePlaylistId($request->get('playlist_id'))->get();
        $songs = [];
        foreach ($playlistLinks as $link) {
            $song = Song::whereId($link->song_id)->get()->first();
            if (!is_null($song)) {
                $songs[] = [
                    'id' => $song->id,
                    'uri' => $song->uri,
                    'queued' => false
                ];
            }
        }
        return response()->json($songs);
    }
}
