<?php

namespace App\Http\Controllers;

use App\DbSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class DbSettingController extends Controller
{
    public static function routes()
    {
        Route::get('/settings')
            ->uses('DbSettingController@showSettings')
            ->name('setting.view');
    }

    public function showSettings(Request $request)
    {
        $settings = DbSetting::all();
        return view('listings.settings')->with(compact('settings'));
    }
}
