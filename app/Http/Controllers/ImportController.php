<?php

namespace App\Http\Controllers;

use App\Http\Preview\ImportInformation;
use App\Http\Preview\ImportPreview;
use App\Permission;
use App\Playlist;
use App\PlaylistSongLink;
use App\Providers\Import\ImportManagerService;
use App\Song;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class ImportController extends Controller
{
    const SKEY_IMPORT = 'importInformation';
    const SKEY_IMPORT_TIME = 'importPreviewTime';
    const PREVIEW_PER_PAGE = 25;
    const IMPORT_GRACE_TIME_SEC = 15;

    public static function routes(): void
    {
        DebugController::registerVolatileSessionField(ImportController::SKEY_IMPORT);

        Route::get('/lib/import')
            ->middleware(
                'auth',
                'permission:' . Permission::URI_IMPORT
            )
            ->uses('ImportController@showImport')
            ->name('playlists.import');

        Route::post('/ajax/previewImportURI')
            ->middleware(
                'auth',
                'permission:' . Permission::URI_IMPORT
            )
            ->uses('ImportController@previewImport')
            ->name('lib.importPreview');

        Route::post('/ajax/importURI')
            ->middleware(
                'auth',
                'permission:' . Permission::URI_IMPORT
            )
            ->uses('ImportController@importUri')
            ->name('lib.import');
    }

    public function showImport()
    {
        return view('import.import');
    }

    /**
     * @param Request $request
     * @param ImportManagerService $importManager
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function previewImport(Request $request, ImportManagerService $importManager)
    {
        $errors = [];
        $importInformation = new Collection();
        $importUri = $request->get('uri');
        $skip = false;

        if (is_null($importUri) || strlen($importUri) < 0) {
            $errors['url'] = 'URL may not be empty!';
            $skip = true;
        }

        if (!$skip) {
            // If the session already contains a result for this URL return it.
            $session = Auth::getSession();
            if ($session->has(ImportController::SKEY_IMPORT)) {

                $cachedInfo = $session->get(ImportController::SKEY_IMPORT);
                if ($cachedInfo->getImportUri() == $importUri) {

                    if ($cachedInfo->getItems()->count() >= 0) {
                        return $this->returnPaginatedView($cachedInfo, $this->getCurrentPage($request), $errors);
                    }
                }
            }
        }
        /* @var  $lastRequestTime Carbon */
        $lastRequestTime = Auth::getSession()->get(ImportController::SKEY_IMPORT_TIME, null);
        $currentTime = Carbon::now();
        if (!is_null($lastRequestTime) && $lastRequestTime->diffInSeconds($currentTime) < ImportController::IMPORT_GRACE_TIME_SEC) {
            $errors['throttle'] = 'To many requests!';
            $skip = true;
        }

        if (!$skip) {
            $importer = $importManager->getImporter($importUri);
            if (is_null($importer) || !$importer->isEnabled()) {
                $skip = true;
                $errors['url'] = 'No import service found matching this URL.';
            }

            if (!$skip) {
                Auth::getSession()->put(ImportController::SKEY_IMPORT_TIME, Carbon::now());
                $importInformation = $importer->getImportInformation($importUri);
                Auth::getSession()->put(ImportController::SKEY_IMPORT, $importInformation);

                if ($importInformation->getItems()->count() <= 0) {
                    $errors['url'] = 'Import returned no possible results';
                }
            }
        }

        return $this->returnPaginatedView($importInformation, $this->getCurrentPage($request), $errors);
    }

    private function returnPaginatedView(ImportInformation $importInfo, int $currentPage, array $errors)
    {
        $usedPreviews = $this->paginate($importInfo->getItems(), $currentPage, ImportController::PREVIEW_PER_PAGE);
        $startNum = ImportController::PREVIEW_PER_PAGE * ($currentPage - 1);
        return view('import.importList')
            ->withErrors($errors)
            ->with([
                'importInformation' => $importInfo,
                'importPreviews' => $usedPreviews,
                'startNum' => $startNum
            ]);
    }

    private function paginate(Collection $items, int $page, int $perPage): Paginator
    {
        $paginator = new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page);
        $paginator = $paginator->withPath('/ajax/previewImportURI');
        return $paginator;
    }

    private function getCurrentPage(Request $request) {
        return intval($request->get('page', '1'));
    }

    public function importUri(Request $request)
    {
        $errors = [];
        $skip = false;
        $uri = $request->get('uri', null);
        if (is_null($uri)) {
            $errors['uri'] = 'URI may not be empty!';
            $skip = true;
        }

        if (!$skip) {
            /* @var $importInformation ImportInformation */
            $importInformation = Auth::getSession()->get(ImportController::SKEY_IMPORT);

            if (is_null($importInformation) || $importInformation->getItems()->count() <= 0) {
                $errors['uri'] = 'No items available to import';
                $skip = true;
            }

            if (!$skip) {
                $tracks = new Collection();
                foreach ($importInformation->getItems() as $preview) {
                    /* @var $preview ImportPreview */

                    $externalId = $preview->getExternalId();
                    // Skip external ids that are unset or about to be imported anyways.
                    if (is_null($externalId) || strlen(trim($externalId)) <= 0
                        || $tracks->has($externalId))
                        continue;

                    // Skip already imported tracks
                    if ($preview->existsInDB())
                        continue;

                    $track = [];
                    $track['title'] = $preview->getTeaserTitle();
                    $track['uri'] = $preview->getOriginalUrl();
                    $track['user_id'] = Auth::user()->id;
                    $track['external_id'] = $preview->getExternalId();
                    $tracks->put($preview->getExternalId(), $track);
                }

                Song::insert($tracks->values()->toArray());

                if ($importInformation->hasPlaylist()
                    && $request->get('create_playlist', false) == true) {

                    $playlistResults = Playlist::whereExternalId($importInformation->getPlaylistId())->get();

                    if ($playlistResults->count() == 1) {
                        $playlist = $playlistResults->get(0);
                    } else {
                        $playlist = new Playlist();
                        $playlist->created_at = Carbon::now();
                        $playlist->user_id = Auth::user()->id;
                        $playlist->display_name = $importInformation->getPlaylistName();
                        $playlist->external_id = $importInformation->getPlaylistId();
                        $playlist->save();
                    }

                    // Clear all songs from the list
                    PlaylistSongLink::wherePlaylistId($playlist->id)->delete();

                    foreach ($importInformation->getItems() as $preview) {
                        if (is_null($externalId) || strlen(trim($externalId)) <= 0)
                            continue;

                        // Note: As we do not do the duplication check here, duplicated entries are supported.

                        PlaylistSongLink::create([
                            'playlist_id' => $playlist->id,
                            'song_id' => Song::whereExternalId($preview->getExternalId())->get()->get(0)->id
                        ]);
                    }

                    Auth::getSession()->forget(ImportController::SKEY_IMPORT);
                }
            }
        }

        return back()->withErrors($errors);
    }
}
