<?php
/**
 * Created by MarkL4YG on 30-Jul-18
 */

namespace App\Http\Preview;


use App\Song;

class ImportPreview
{
    protected $originalUrl;
    protected $externalId;
    protected $teaserTitle;
    protected $teaserImage;
    protected $teaserDescription;
    protected $maxDescriptionLength = 280;

    /**
     * @return mixed
     */
    public function getOriginalUrl()
    {
        return $this->originalUrl;
    }

    /**
     * @param mixed $originalUrl
     */
    public function setOriginalUrl($originalUrl): void
    {
        $this->originalUrl = $originalUrl;
    }

    /**
     * @return mixed
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @param mixed $externalId
     */
    public function setExternalId($externalId): void
    {
        $this->externalId = $externalId;
    }

    /**
     * @return mixed
     */
    public function getTeaserTitle()
    {
        return $this->teaserTitle;
    }

    /**
     * @param mixed $teaserTitle
     */
    public function setTeaserTitle($teaserTitle): void
    {
        $this->teaserTitle = $teaserTitle;
    }

    /**
     * @return mixed
     */
    public function getTeaserImage()
    {
        return $this->teaserImage;
    }

    /**
     * @param mixed $teaserImage
     */
    public function setTeaserImage($teaserImage): void
    {
        $this->teaserImage = $teaserImage;
    }

    /**
     * @return mixed
     */
    public function getTeaserDescription()
    {
        if (strlen($this->teaserDescription) > $this->maxDescriptionLength) {
            return substr($this->teaserDescription, 0, $this->maxDescriptionLength - 3).'...';
        }
        return $this->teaserDescription;
    }

    /**
     * @param mixed $teaserDescription
     */
    public function setTeaserDescription($teaserDescription): void
    {
        $this->teaserDescription = $teaserDescription;
    }

    public function existsInDB(): bool
    {
        return !is_null($this->externalId) && strlen($this->externalId) > 0 && Song::whereExternalId($this->externalId)->get()->count() > 0;
    }
}