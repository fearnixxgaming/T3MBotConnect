<?php
/**
 * Created by MarkL4YG on 07-Aug-18
 */

namespace App\Http\Preview;


use Illuminate\Support\Collection;

class ImportInformation
{
    protected $importUri;
    protected $items;
    protected $playlistName;
    protected $playlistId;

    /**
     * ImportInformation constructor.
     * @param $items
     */
    public function __construct()
    {
        $this->items = new Collection();
    }

    /**
     * @return mixed
     */
    public function getImportUri(): string
    {
        return $this->importUri;
    }

    /**
     * @param mixed $importUri
     */
    public function setImportUri($importUri): void
    {
        $this->importUri = $importUri;
    }

    /**
     * @return Collection
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    /**
     * @return mixed
     */
    public function getPlaylistName(): ?string
    {
        return $this->playlistName;
    }

    /**
     * @param mixed $playlistName
     */
    public function setPlaylistName($playlistName): void
    {
        $this->playlistName = $playlistName;
    }

    /**
     * @return mixed
     */
    public function getPlaylistId(): ?string
    {
        return $this->playlistId;
    }

    /**
     * @param mixed $playlistId
     */
    public function setPlaylistId($playlistId): void
    {
        $this->playlistId = $playlistId;
    }

    public function hasPlaylist(): bool
    {
        return !is_null($this->playlistName) && strlen(trim($this->playlistName)) > 0
            && !is_null($this->playlistId) && strlen(trim($this->playlistId)) > 0;
    }
}