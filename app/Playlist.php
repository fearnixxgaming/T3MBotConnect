<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Playlist
 *
 * @property int $id
 * @property int $user_id
 * @property int $is_public
 * @property string $display_name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Playlist whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Playlist whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Playlist whereIsPublic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Playlist whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Playlist whereUserId($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Playlist whereNotUserId($user_id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Playlist whereDisplayName($value)
 * @property string $external_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Playlist whereExternalId($value)
 */
class Playlist extends Model
{
    //

    public function scopeWhereNotUserId(Builder $query, int $user_id)
    {
        return $query->where('user_id', '!=', $user_id);
    }

    public function containsSong(int $song_id) :bool
    {
        return !is_null(PlaylistSongLink::wherePlaylistId($this->id)->whereSongId($song_id)->get()->first());
    }

    public function getPublicDisplayName(): string
    {
        return sprintf('%s [%s]', $this->display_name, User::whereId($this->user_id)->get()->get(0)->name);
    }
}
