<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\MusicBotLock
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $created_on
 * @property string $refreshed_on
 * @property int $mbot_id
 * @property int $user_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MusicBotLock whereCreatedOn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MusicBotLock whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MusicBotLock whereMbotId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MusicBotLock whereRefreshedOn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MusicBotLock whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MusicBotLock whereNotShared()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MusicBotLock whereNotOutdated()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MusicBotLock whereOutdated()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MusicBotLock whereShared()
 * @property int|null $shared_via
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MusicBotLock whereSharedVia($value)
 */
class MusicBotLock extends Model
{
    protected $table = 'musicbots_locks';

    public $timestamps = false;

    const STATES = [
        "FREE" => 0,
        "OWNED" => 1,
        "SHARED" => 2,
        "LOCKED" => 3,
        "OFFLINE" => 4
    ];
    const STATES_STRINGS = [
        "unlocked",
        "locked_available",
        "locked_shared",
        "locked_unavailable",
        "locked_offline"
    ];

    public static function scopeWhereNotShared(Builder $query)
    {
        return $query->whereNull('shared_via');
    }

    public static function scopeWhereShared(Builder $query)
    {
        return $query->whereNotNull('shared_via');
    }

    public static function scopeWhereNotOutdated(Builder $query)
    {
        $refresh = Carbon::now(config('app.timezone'))->subMinutes(5);
        return $query->where('refreshed_on', '>', $refresh);
    }

    public static function scopeWhereOutdated(Builder $query)
    {
        $refresh = Carbon::now(config('app.timezone'))->subMinutes(5);
        return $query->where('refreshed_on', '<=', $refresh);
    }
}
