<?php

namespace App;

use Illuminate\Database\Query\Builder;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * App\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string|null $remember_token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User byEMail($email)
 * @property int $activated
 * @property int $banned
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereActivated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereBanned($value)
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isGroupMember(int $group_id): bool
    {
        return GroupLink::whereGroupId($group_id)->whereUserId($this->id)->get()->count() > 0;
    }

    public function isAdmin(): bool
    {
        return $this->checkPerm(Permission::ADMIN);
    }

    public function hasPermission(string $perm): bool
    {
        return $this->isAdmin() || $this->checkPerm($perm);
    }

    public function checkPerm(string $perm): bool
    {
        $links = GroupLink::whereUserId($this->id)->get();
        foreach ($links as $link) {
            $group = Group::whereId($link->group_id)->get()->first();
            if (!is_null($group) && $group->hasPermission($perm)) {
                return true;
            }
        }
        return false;
    }

    public function getCurrentMBot(): ?MusicBot
    {
        $currentLock = MusicBotLock::whereUserId($this->id)->whereNotOutdated()->get()->first();
        if (!is_null($currentLock)) {
            return MusicBot::whereId($currentLock->mbot_id)->get()->first();
        }
        return null;
    }

    public function hasCurrentMBot(): bool
    {
        return !is_null($this->getCurrentMBot());
    }
}
