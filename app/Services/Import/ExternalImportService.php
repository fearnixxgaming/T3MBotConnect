<?php
/**
 * Created by MarkL4YG on 30-Jul-18
 */

namespace App\Services\Import;

use App\DbSetting;
use App\Http\Preview\ImportInformation;
use Illuminate\Support\Collection;

abstract class ExternalImportService
{
    public $IDENTIFIER = null;
    protected $readDB = false;
    protected $enabled = true;
    protected $api_key;

    public function boot()
    {

    }

    private function check(): void
    {
        if ($this->readDB === false) {
            $dbSetting = DbSetting::whereIdent(DbSetting::EXT_SVC_DISABLE . $this->IDENTIFIER)->get();
            if ($dbSetting->count() == 1 && $dbSetting->get(0)->getValue() == true) {
                $this->enabled = false;
            }
            $this->readDB = true;
        }
    }

    public function isEnabled(): bool
    {
        $this->check();
        return $this->enabled === true;
    }

    public abstract function matches(string $importUrl): bool;

    public abstract function getImportInformation(string $importUrl): ImportInformation;
}