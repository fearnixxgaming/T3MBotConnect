<?php
/**
 * Created by MarkL4YG on 30-Jul-18
 */

namespace App\Services\Import;

use App\Exceptions\MBotException;
use App\Exceptions\UnmatchedUrlException;
use App\Http\Controllers\ImportController;
use App\Http\Preview\ImportInformation;
use App\Http\Preview\ImportPreview;
use Curl\Curl;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class YouTubeApiImportService extends ExternalImportService
{
    public $IDENTIFIER = "youtube";
    const URL_PATTERN_PLAYLIST = '/(youtube\.com)\/playlist.+list=(.*)/';
    const API_LIST_ITEMS = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId=%s&maxResults=%s&key=%s";
    const API_LIST_INFO = "https://www.googleapis.com/youtube/v3/playlists?part=snippet&id=%s&key=%s";
    const URL_PATTERN_VIDEO = '/(youtube\.com)\/watch.+v=(.*)/';
    const API_SHOW_VIDEO = 'https://www.googleapis.com/youtube/v3/videos?part=snippet&id=%s&key=%s';

    public function boot(): void
    {
        parent::boot();

        $api_key = env('API_GOOGLE_KEY', null);
        if (!is_null($api_key)) {
            $this->api_key = $api_key;
        } else {
            $this->enabled = false;
        }
    }

    public function matches(string $importUrl): bool
    {
        return preg_match(YouTubeApiImportService::URL_PATTERN_PLAYLIST, $importUrl)
            || preg_match(YouTubeApiImportService::URL_PATTERN_VIDEO, $importUrl);
    }

    public function getImportInformation(string $importUrl): ImportInformation
    {

        $matches = [];
        if (preg_match(YouTubeApiImportService::URL_PATTERN_PLAYLIST, $importUrl, $matches)
            && count($matches) > 0) {
            $listId = $matches[2];
            $importInformation = $this->getAllListItems($listId, 50);
            $importInformation->setImportUri($importUrl);
            return $importInformation;
        }

        $matches = [];
        if (preg_match(YouTubeApiImportService::URL_PATTERN_VIDEO, $importUrl, $matches)
            && count($matches) > 0) {
            $videoId = $matches[2];
            $importInformation = $this->getVideoItems($videoId);
            $importInformation->setImportUri($importUrl);
            return $importInformation;
        }

        throw new UnmatchedUrlException("Uri does not match any supported YouTube URI!");
    }

    private function getVideoItems(string $videoId)
    {
        $importInformation = new ImportInformation();
        $url = sprintf(YouTubeApiImportService::API_SHOW_VIDEO, $videoId, $this->api_key);
        Log::debug("YTAPI requests: " . $url);

        $requestCurl = new Curl();
        $requestCurl->get($url);

        if ($requestCurl->error) {
            $requestCurl->close();
            Log::warning("YTAPI request returned error: " . $requestCurl->error_message);
            return $importInformation;
        }
        $json = json_decode($requestCurl->response, true);
        $requestCurl->close();
        $items = $importInformation->getItems();
        $this->addItemsToCollection($json, $items);
        return $importInformation;
    }

    private function getAllListItems(string $playlistId, int $perRequest): ImportInformation
    {
        $importInformation = new ImportInformation();

        $info = $this->getPlaylistInfo($playlistId);
        if (count($info) > 0 && array_key_exists('title', $info)) {
            $importInformation->setPlaylistName($info['title']);
            $importInformation->setPlaylistId('youtube:playlist:' . $info['id']);
        }

        // Non-null initial value to perform first page request
        $nextPage = '';
        $url = sprintf(YouTubeApiImportService::API_LIST_ITEMS, $playlistId, $perRequest, $this->api_key);
        Log::debug("YTAPI requests: " . $url);

        while ($nextPage !== null) {
            $requestUrl = $url;
            if (strlen($nextPage) > 0) {
                $requestUrl .= '&pageToken=' . $nextPage;
            }
            $requestCurl = new Curl();
            $requestCurl->get($requestUrl);

            if ($requestCurl->error) {
                $requestCurl->close();
                Log::warning("YTAPI request returned error: " . $requestCurl->error_message);
                break;
            }
            $json = json_decode($requestCurl->response, true);
            $requestCurl->close();

            if (array_key_exists('nextPageToken', $json)
                && strlen(trim($json['nextPageToken'])) > 0) {
                $nextPage = $json['nextPageToken'];
            } else {
                $nextPage = null;
            }

            $items = $importInformation->getItems();
            $this->addItemsToCollection($json, $items);
        }
        return $importInformation;
    }

    private function getPlaylistInfo(string $playlistId) :array
    {
        $url = sprintf(YouTubeApiImportService::API_LIST_INFO, $playlistId, $this->api_key);
        Log::debug("YTAPI request: " . $url);
        $requestCurl = new Curl();
        $requestCurl->get($url);

        if ($requestCurl->error) {
            $requestCurl->close();
            Log::warning("YTAPI request returned error: " . $requestCurl->error_message);
            return [];
        }

        $json = json_decode($requestCurl->response, true);
        $requestCurl->close();

        if (array_key_exists('items', $json)) {
            foreach ($json['items'] as $item) {
                if (!is_null($item) && $item['kind'] == 'youtube#playlist') {
                    $item['snippet']['id'] = $item['id'];
                    return $item['snippet'];
                }
            }
        }
        return [];
    }

    private function addItemsToCollection(array $json, Collection &$collection)
    {
        foreach ($json['items'] as $item) {

            $kind = $item['kind'];

            if ($kind === 'youtube#playlistItem') {
                $this->addPlaylistItemToCollection($collection, $item);
            } else if ($kind === 'youtube#video') {
                $this->addVideoItemToCollection($collection, $item);
            }
        }
    }

    /**
     * @param Collection $collection
     * @param $item
     */
    private function addPlaylistItemToCollection(Collection &$collection, array $item): void
    {
        $playlistItem = $item['snippet'];
        $previewItem = new ImportPreview();
        $previewItem->setTeaserTitle($playlistItem['title']);
        $description = $playlistItem['description'];
        $description = str_replace('\n', "\n", $description);
        $previewItem->setTeaserDescription($description);

        if (array_key_exists('thumbnails', $playlistItem)) {
            $previewItem->setTeaserImage($playlistItem['thumbnails']['default']['url']);
        }

        $resourceId = $playlistItem['resourceId'];
        if ($resourceId['kind'] == 'youtube#video') {
            $videoId = $resourceId['videoId'];
            $previewItem->setExternalId('youtube:video:' . $videoId);

            $videoUrl = 'https://youtube.com/watch?v=' . $videoId;
            $previewItem->setOriginalUrl($videoUrl);
        }
        $collection->push($previewItem);
    }

    public function addVideoItemToCollection(Collection &$collection, array $item): void
    {
        $videoItem = $item['snippet'];
        $previewItem = new ImportPreview();
        $previewItem->setTeaserTitle($videoItem['title']);
        $description = $videoItem['description'];
        $description = str_replace('\n', "\n", $description);
        $previewItem->setTeaserDescription($description);

        if (array_key_exists('thumbnails', $videoItem)) {
            $previewItem->setTeaserImage($videoItem['thumbnails']['default']['url']);
        }

        $videoId = $item['id'];
        $videoUrl = 'https://youtube.com/watch?v=' . $videoId;
        $previewItem->setExternalId('youtube:video:' . $videoId);
        $previewItem->setOriginalUrl($videoUrl);
        $collection->push($previewItem);
    }
}