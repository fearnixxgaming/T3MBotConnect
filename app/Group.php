<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * App\Group
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $display_name
 * @property int $default
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Group whereDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Group whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Group whereId($value)
 */
class Group extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'display_name', 'default'
    ];

    public function isAdmin(): bool
    {
        $perm = Permission::wherePermission(Permission::ADMIN)->get()->get(0);
        return PermissionLink::whereGroupId($this->id)->wherePermId($perm->id)->get()->count() > 0;
    }

    public function hasPermission(string $perm)
    {
        $perm = Permission::wherePermission($perm)->first();
        return !is_null($perm) && $this->hasPerm($perm->id);
    }

    public function hasPerm(int $perm_id)
    {
        return PermissionLink::whereGroupId($this->id)->wherePermId($perm_id)->get()->count() > 0;
    }
}
