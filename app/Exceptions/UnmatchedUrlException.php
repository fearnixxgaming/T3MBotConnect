<?php
/**
 * Created by PhpStorm.
 * User: life4
 * Date: 01-Aug-18
 * Time: 4:43 PM
 */

namespace App\Exceptions;


class UnmatchedUrlException extends \Exception
{

    /**
     * UnmatchedUrlException constructor.
     */
    public function __construct(string $message)
    {
        parent::__construct($message);
    }
}