<?php
/**
 * Created by PhpStorm.
 * User: life4
 * Date: 26-Feb-18
 * Time: 9:24 PM
 */

namespace App\Exceptions;

use Throwable;

class MBotTalkException extends MBotException
{
    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
