<?php
/**
 * Created by MarkL4YG on 30-Jul-18
 */

namespace App\Providers\Import;

use App\Services\Import\YouTubeApiImportService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class YouTubeImportProvider extends ServiceProvider
{
    private $youtubeImporter;

    public function __construct(Application $app)
    {
        parent::__construct($app);
    }

    public function boot(ImportManagerService $importService)
    {
        $this->youtubeImporter = new YouTubeApiImportService();
        $this->youtubeImporter->boot();
        $importService->registerImporter($this->youtubeImporter);
    }

    public function register(): void
    {
        $this->app->singleton(YouTubeApiImportService::class, function () {
            return $this->youtubeImporter;
        });
    }
}