<?php
/**
 * Created by MarkL4YG on 30-Jul-18
 */

namespace App\Providers\Import;

use App\Services\Import\ExternalImportService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;

class ImportManagerService extends ServiceProvider
{
    private $importers;
    private $logger;

    public function __construct(Application $app)
    {
        parent::__construct($app);
    }

    public function boot(Log $logger)
    {
        $this->importers = new Collection();
        $this->logger = $logger;
    }

    public function register()
    {
        $this->app->singleton(ImportManagerService::class, function () {
            return $this;
        });
    }

    public function registerImporter(ExternalImportService $provider): void
    {
        $this->importers[] = $provider;
    }

    public function getImporter(string $url): ?ExternalImportService
    {
        foreach ($this->importers as $importer) {
            /* @var $importer ExternalImportService */
            if ($importer->matches($url)) {
                return $importer;
            }
        }
        return null;
    }
}