<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\GroupLink
 *
 * @mixin \Eloquent
 * @property int $id
 * @property int $group_id
 * @property int $user_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GroupLink whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GroupLink whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GroupLink whereUserId($value)
 */
class GroupLink extends Model
{
    protected $table = "groups_links";

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'group_id'
    ];
}
