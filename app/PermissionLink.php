<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\PermissionLink
 *
 * @mixin \Eloquent
 * @property int $id
 * @property int $group_id
 * @property int $perm_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PermissionLink whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PermissionLink whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PermissionLink wherePermId($value)
 */
class PermissionLink extends Model
{
    protected $table = 'permissions_links';

    public $timestamps = false;
}
