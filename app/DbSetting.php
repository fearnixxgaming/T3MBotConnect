<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\DbSetting
 *
 * @property int $id
 * @property string $ident
 * @property string|null $value
 * @property string $default
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DbSetting whereDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DbSetting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DbSetting whereIdent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DbSetting whereValue($value)
 * @mixin \Eloquent
 */
class DbSetting extends Model
{
    const PP_MUSICBOTS = 'pagination_musicbots_pp';
    const PP_SONGS = 'pagination_songs_pp';
    const PP_PLAYLISTS = 'pagination_playlists_pp';
    const PP_USERS = 'pagination_users_pp';
    const PP_SETTINGS = 'pagination_settings_pp';

    const EXT_SVC_DISABLE = 'disable_externalSvc_';

    protected $table = 'settings';

    public $timestamps = false;

    public function getValue()
    {
        if (is_null($this->value) || strlen($this->value) === 0) {
            return $this->default;
        }
        return $this->value;
    }
}
