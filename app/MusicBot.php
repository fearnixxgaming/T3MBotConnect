<?php

namespace App;

use App\Exceptions\MBotTalkException;
use App\Exceptions\UnexpectedValueException;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

/**
 * App\MusicBot
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $url
 * @property int $port
 * @property string $admin_pass
 * @property string $display_name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MusicBot whereAdminPass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MusicBot whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MusicBot whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MusicBot wherePort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MusicBot whereUrl($value)
 */
class MusicBot extends Model
{
    const ENDPOINTS = [
        "volume" => "getproperty?prop=volume&find=ANS_volume",
        "length" => "getproperty?prop=length&find=ANS_length",
        "playing" => "getnowPlaying",
        "queue" => "getqueue",
        "queue_count" => "getqueuecount",
        "control" => [
            "stop" => "stop",
            "next" => "next",
            "previous" => "prev",
            "mute" => "pause",
            "volume" => "setvolume?vol=%volume%",
            "queue_url" => "queueadd?mode=youtube&source=%url%",
            "play_url" => "playyoutube?link=%url%",
            "play_file" => "play?folder=%folder%&file=%file%",
            "play_playlist" => "play?folder=playlist&file=%file%",
            "play_playlist_random" => "playplaylistrandom?pl=%file%",
            "channel" => "switchchannel?id=%channelID%&password=%password%"
        ]
    ];

    protected $table = 'musicbots';

    public $timestamps = false;

    public function getLockState(): int
    {
        $lock = MusicBotLock::whereMbotId($this->id)->whereUserId(Auth::user()->id)->whereNotOutdated()->whereNotShared()->get()->first();
        if (is_null($lock)) {
            $lock = MusicBotLock::whereMbotId($this->id)->whereUserId(Auth::user()->id)->whereNotOutdated()->whereShared()->get()->first();
            if (is_null($lock)) {
                $lock = MusicBotLock::whereMbotId($this->id)->whereNotOutdated()->get()->first();
                if (is_null($lock)) {
                    return $this->ping() ? MusicBotLock::STATES['FREE'] : MusicBotLock::STATES['OFFLINE'];
                }
                return MusicBotLock::STATES['LOCKED'];
            }
            return MusicBotLock::STATES['SHARED'];
        } else {
            return MusicBotLock::STATES['OWNED'];
        }
    }

    public function getLockStateStr(): string
    {
        return MusicBotLock::STATES_STRINGS[$this->getLockState()];
    }

    protected function authorizeWithBackend(): string
    {
        $url = $this->url . ':' . $this->port . '/login';
        logger('Trying to login to: ' . $url);

        $data = [
            'password' => $this->admin_pass
        ];

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, 1);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 4);
        curl_setopt($curl, CURLOPT_TIMEOUT, 6);
        $response = curl_exec($curl);

        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ($status === 302 || $status === 200) {
            $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
            $header = substr($response, 0, $header_size);
            $cookiePos = strpos($header, 'authorization_');
            $cookieEndPos = strpos($header, "\n", $cookiePos);
            $authCookie = substr($header, $cookiePos, $cookieEndPos - $cookiePos);

            if (strpos($authCookie, 'authorization_') === 0) {
                return $authCookie;
            } else {
                throw new MBotTalkException('Login did not provide authorization cookie!');
            }
        } else {
            $message = 'Failed to login to music bot! HTTP:' . $status . ' Curl:' . curl_errno($curl) . '-' . curl_error($curl);
            throw new MBotTalkException($message);
        }
    }

    protected function getAuthorizeCookie(): string
    {
        $currentTime = Carbon::now();
        $authCacheKey = 'mbot::auth::' . $this->id;
        $timeCacheKey = 'mbot::authTSP::' . $this->id;

        $cachedAuth = Cache::get($authCacheKey, null);
        $cachedTime = Cache::get($timeCacheKey, null);

        if ($cachedAuth === null ||$cachedTime === null
            || $cachedTime->lt($currentTime->subHour())) {
            $cachedAuth = $this->authorizeWithBackend();
            $cachedTime = $currentTime;

            Cache::put($authCacheKey, $cachedAuth);
            Cache::put($timeCacheKey, $cachedTime->serialize());
        }

        return $cachedAuth;
    }

    protected function getCurl(string $endpoint, array $vars = [])
    {
        $url = $this->url . ':' . $this->port . '/' . $endpoint;
        foreach ($vars as $var => $val) {
            $url = str_replace('%' . $var . '%', $val, $url);
        }
        logger($url);
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_COOKIE, $this->getAuthorizeCookie());
        curl_setopt($curl, CURLOPT_TIMEOUT_MS, 20000);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT_MS, 10000);
        return $curl;
    }

    protected function sendExpectAnswer($curl, array $expect = [])
    {
        $answer = curl_exec($curl);
        if ($answer === false) {
            //throw new MBotTalkException("Curl-error: ".curl_errno($curl).'-'.curl_error($curl).' for '.curl_getinfo($curl, CURLINFO_EFFECTIVE_URL));
            throw new MBotTalkException("Curl-error: " . curl_errno($curl) . '-' . curl_error($curl));
        }
        $isConstr = '';
        $containsConstr = '';
        $maxConstr = -1;
        $minConstr = -1;
        $isFailed = array_key_exists('is', $expect) && $answer !== ($isConstr = $expect['is']);
        $containsFailed = array_key_exists('contains', $expect) && strpos($answer, ($containsConstr = $expect['contains'])) == false;
        $minLengthFailed = array_key_exists('min_length', $expect) && strlen($answer) < ($minConstr = $expect['min_length']);
        $maxLengthFailed = array_key_exists('max_length', $expect) && strlen($answer) > ($maxConstr = $expect['max_length']);
        if ($isFailed && $containsFailed) {
            throw new UnexpectedValueException('Expect/contains "' . $isConstr . '"/"' . $containsConstr . '" got "' . $answer . '"');
        }
        if ($maxLengthFailed || $minLengthFailed) {
            throw new UnexpectedValueException('Expected length mismatch: Min/Max "' . $minConstr . '"/"' . $maxConstr . '" got "' . strlen($answer) . '"');
        }
        return $answer;
    }

    public function ping(): bool
    {
        try {
            $vol = $this->getVolume();
            return is_numeric($vol) && $vol >= 0;
        } catch (\Exception $e) {
            logger('Failed to ping bot!' . $e);
            return false;
        }
    }

    public function getVolume(): int
    {
        $curl = $this->getCurl(MusicBot::ENDPOINTS['volume']);
        $answer = $this->sendExpectAnswer($curl, ['contains' => '=']);
        return intval(explode('=', $answer)[1]);
    }

    public function getTrackLength(): int
    {
        $curl = $this->getCurl(MusicBot::ENDPOINTS['length']);
        $answer = $this->sendExpectAnswer($curl, ['contains' => '=', 'is' => 'NA']);
        if (strpos($answer, '=') != false) {
            $len = explode('=', $answer)[1];
            return strlen($len) > 0 ? intval($len) : 0;
        } elseif ($answer === 'NA') {
            return 0;
        }
    }

    public function getTrackName(): string
    {
        $curl = $this->getCurl(MusicBot::ENDPOINTS['playing']);
        $answer = $this->sendExpectAnswer($curl);
        $answer = (strlen($answer) > 0 ? $answer : 'n/a');
        return $answer;
    }

    public function getQueueCount(): int
    {
        $curl = $this->getCurl(MusicBot::ENDPOINTS['queue_count']);
        $answer = $this->sendExpectAnswer($curl, ['min_length' => 1]);
        return intval($answer);
    }

    public function getQueue(): array
    {
        $curl = $this->getCurl(MusicBot::ENDPOINTS['queue']);
        $answer = $this->sendExpectAnswer($curl, ['min_length' => 0]);
        $queue = explode(';', $answer);
        return $queue;
    }

    public function stop(): bool
    {
        $curl = $this->getCurl(MusicBot::ENDPOINTS['control']['stop']);
        $answer = $this->sendExpectAnswer($curl, ['is' => '.']);
        return true;
    }

    public function playURL(string $url)
    {
        $curl = $this->getCurl(MusicBot::ENDPOINTS['control']['play_url'], ["url" => $url]);
        $answer = $this->sendExpectAnswer($curl, ['is' => '.']);
        return true;
    }

    public function queueURL(string $url)
    {
        $curl = $this->getCurl(MusicBot::ENDPOINTS['control']['queue_url'], ["url" => urldecode($url)]);
        $answer = $this->sendExpectAnswer($curl, ['is' => '.']);
        return true;
    }

    public function next()
    {
        $curl = $this->getCurl(MusicBot::ENDPOINTS['control']['next']);
        $answer = $this->sendExpectAnswer($curl, ['is' => '.']);
        return true;
    }

    public function setVolume(int $volume)
    {
        $curl = $this->getCurl(MusicBot::ENDPOINTS['control']['volume'], ['volume' => $volume]);
        $answer = $this->sendExpectAnswer($curl, ['is' => '.']);
        return true;
    }

    public function mute()
    {
        $curl = $this->getCurl(MusicBot::ENDPOINTS['control']['mute']);
        $answer = $this->sendExpectAnswer($curl, ['is' => '.']);
        return true;
    }

    public function previous()
    {
        $curl = $this->getCurl(MusicBot::ENDPOINTS['control']['previous']);
        $answer = $this->sendExpectAnswer($curl, ['is' => '.']);
        return true;
    }

    public function sendChannel(int $channelId)
    {
        $curl = $this->getCurl(MusicBot::ENDPOINTS['control']['channel'], ["channelID" => $channelId, "password" => ""]);
        $answer = $this->sendExpectAnswer($curl, ['is' => "."]);
        return true;
    }
}
