<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Permission
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $permission
 * @property int $category
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission wherePermission($value)
 */
class Permission extends Model
{
    public $timestamps = false;

    const BOTS_VIEW = "mbot_view";
    const BOTS_CONTROL = "mbot_control";
    const BOTS_EDIT = "mbot_edit";

    const USERS_VIEW = "users_view";
    const USERS_EDIT = "user_edit";
    const USERS_ASSIGN = "users_assign";

    const SONG_VIEW = "song_view";
    const SONG_REGISTER = "song_register";

    const PLAYLIST_VIEW = "playlist_view";
    const PLAYLIST_CREATE_PRIVATE = "playlist_create_private";
    const PLAYLIST_CREATE_PUBLIC = "playlist_create_public";
    const PLAYLIST_MODIFY_UNOWNED = "playlist_modify_unowned";

    const URI_IMPORT = "uri_import";

    const GROUPS_VIEW = "groups_view";
    const GROUPS_EDIT = "groups_edit";

    const SETTINGS_EDIT = "settings_edit";
    const ADMIN = "user_is_admin";

    const ALL = [
        Permission::BOTS_VIEW,
        Permission::BOTS_CONTROL,
        Permission::BOTS_EDIT,
        Permission::USERS_VIEW,
        Permission::USERS_EDIT,
        Permission::USERS_ASSIGN,
        Permission::SONG_VIEW,
        Permission::SONG_REGISTER,
        Permission::PLAYLIST_VIEW,
        Permission::PLAYLIST_CREATE_PRIVATE,
        Permission::PLAYLIST_CREATE_PUBLIC,
        Permission::PLAYLIST_MODIFY_UNOWNED,
        Permission::URI_IMPORT,
        Permission::GROUPS_VIEW,
        Permission::GROUPS_EDIT,
        Permission::SETTINGS_EDIT,
        Permission::ADMIN
    ];
}
