<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\TS3ChannelProperty
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TS3ChannelProperty whereChannelId($channelId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TS3ChannelProperty whereIdent($ident)
 * @mixin \Eloquent
 */
class TS3ChannelProperty extends Model
{
    protected $connection = 'ts3server';

    protected $table = 'channel_properties';

    public function scopeWhereChannelId(Builder $query, int $channelId)
    {
        return $query->where('id', '=', $channelId);
    }

    public function scopeWhereIdent(Builder $query, string $ident)
    {
        return $query->where('ident', '=', $ident);
    }
}
