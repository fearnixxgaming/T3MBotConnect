<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\PlaylistSongLink
 *
 * @property int $id
 * @property int $playlist_id
 * @property int $song_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PlaylistSongLink whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PlaylistSongLink wherePlaylistId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PlaylistSongLink whereSongId($value)
 * @mixin \Eloquent
 */
class PlaylistSongLink extends Model
{
    //

    public $timestamps = false;

    protected $table = 'playlists_song_links';

    protected $fillable = ['playlist_id', 'song_id'];
}
