<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * App\TS3Channel
 *
 * @mixin \Eloquent
 */
class TS3Channel extends Model
{
    protected $connection = "ts3server";

    protected $table = "channels";

    private $children = null;

    public function getProperty(string $ident): ?string
    {
        return TS3ChannelProperty::whereChannelId($this->channel_id)->whereIdent($ident)->get()->first()->value;
    }

    public function sortChildren(): void
    {
        if (!is_null($this->children)) {
        }
    }

    public function addChild(TS3Channel $channel): void
    {
        if (is_null($this->children)) {
            $this->children = collect([]);
        }
        $this->children->put($channel->channel_id, $channel);
    }

    public function setChildren(Collection $children): void
    {
        $this->children = $children;
    }

    public function getChildren(): Collection
    {
        if (is_null($this->children)) {
            $this->children = collect([]);
        }
        return $this->children;
    }
}
