<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\LockShareRequest
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LockShareRequest whereNotIgnored()
 * @mixin \Eloquent
 * @property int $id
 * @property int $user_id
 * @property int $mbot_id
 * @property int $ignored
 * @property string $created_on
 * @property string $refreshed_on
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LockShareRequest whereCreatedOn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LockShareRequest whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LockShareRequest whereIgnored($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LockShareRequest whereMbotId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LockShareRequest whereRefreshedOn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LockShareRequest whereUserId($value)
 */
class LockShareRequest extends Model
{
    protected $table = 'musicbots_locks_requests';

    public $timestamps = false;

    public static function scopeWhereNotIgnored(Builder $query)
    {
        return $query->where('ignored', '=', false);
    }
}
