@extends('layouts.master')

@section('content')
    @if(Auth::check())
        <div class="alert alert-success">
            <strong>Success</strong> You're logged in!
            {{ header('refresh:3;url='.URL::to('/')) }}
        </div>
    @else
        <div>
            <h1>Login</h1>
            @include('forms.register')
        </div>
    @endif
@endsection