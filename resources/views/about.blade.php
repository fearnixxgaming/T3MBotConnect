@extends('layouts.master')

@section('content')
    <hr>
    <h1>T3MBotConnect</h1>
    <p>A web interface for the TS3 (music) bots from <a href="https://ts3musicbot.net">ts3musicbot.net</a>
        and <a href="https://musicbot4you.net">musicbot4you.net</a> in a customizable fashion.
    </p>
    <p>
        This project is made using reverse-engineered information. No code has been taken from the original panel.
    </p>
    <p>Project version: {{ config('app.version', 'beta-0.2.1') }}</p>
    <p>Project framework: Laravel 5.6</p>
    <p>Project repository: <a href="https://gitlab.com/fearnixxgaming/t3mbotconnect">git@gitlab.com:fearnixxgaming/t3mbotconnect</a></p>
    <hr/>
    <p>3rd party open-source licenses: &lt To be filled in &gt</p>
@endsection
