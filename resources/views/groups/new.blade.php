@extends('layouts.master')

@section('content')
    <div class="container panel panel-default">
        <h4>Create new group</h4>
        <div class="row">
            <div class="col-md-6 col-xs-12">
                <form method="POST" action="{{ route('group.new') }}">
                    {{ csrf_field() }}
                    <div class="input-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <span class="input-group-addon">Name</span>
                        <input value="{{ old('name') }}" class="form-control" type="text" name="name" placeholder="SomeGroupName" required autofocus>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection