@extends('layouts.master')

@section('content')
    <div class="panel panel-default">
        <div class="row">
            <div class="col-md-5 col-xs-12">
                <h4>Data</h4>
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th>Name</th>
                            <td>
                                {{ $group->display_name }}
                                @if($group->isAdmin())
                                    <span class="label label-danger">Admin</span>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-md-5 col-xs-12">
                <h4>Permissions</h4>
                @if($errors->hasAny('update_count'))
                  <div class="alert alert-success">
                     {{ 'Updated '.$errors->get('update_count')[0].' values.' }}
                  </div>
                @endif
                <form id="perms-for-group" method="POST" action="{{ route('group.edit.perms') }}">
                  {{ csrf_field() }}
                  <input type="hidden" name="group_id" value="{{ $group->id }}">
                    <ul class="list-unstyled">
                        @foreach($perms as $perm)
                            <li>
                                <div class="checkbox">
                                    <label><input type="checkbox" {{ $group->hasPerm($perm->id) ? "checked=checked" : ""}}" name="perm-{{ $perm->id }}">{{ $perm->permission }}</label>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                    <div class="btn btn-primary" onclick="$('#perms-for-group').submit()">
                        Save
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
