@extends('layouts.master')

@section('content')
    <div class="panel panel-default">
        <div class="row">
            <div class="table-responsive">
                <form id="group-delete" class="form-invisible" method="POST" action="{{ route('user.delete') }}">
                    {{ csrf_field() }}
                    <input id="group-delete-id" type="hidden" name="group_id" value="0">
                    <script>
                        function submitDelete(group_id) {
                            $('#group-delete-id').val(group_id);
                            $('#group-delete').submit();
                        }
                    </script>
                </form>
                <table class="table table-unstyled">
                    <tr>
                        <th>Name</th>
                        @if(Auth::user()->isAdmin())
                            <th>Actions</th>
                        @endif
                    </tr>
                    @foreach($groups as $group)
                        <tr>
                            <td class="selectable" onclick="location.href = '{{ route('group.show', $group->id) }}'">
                                <span>{{ $group->display_name }}</span>
                                @if($group->isAdmin())
                                    <span class="label label-danger">Admin</span>
                                @endif
                            </td>
                            @if(Auth::user()->isAdmin())
                                <td>
                                    <button class="btn btn-danger btn-small" onclick="submitDelete({{ $group->id }})">Delete</button>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
        @if(Auth::user()->isAdmin())
            <div class="row">
                <div class="col-md-12 panel-default">
                    <a class="link-unstyled" href="{{ route('group.register') }}">
                    <span class="btn btn-success btn-small">
                        Add new
                    </span>
                    </a>
                </div>
            </div>
        @endif
    </div>
@endsection