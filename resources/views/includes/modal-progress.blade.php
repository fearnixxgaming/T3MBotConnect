<!-- Modal -->
<div id="modal-progress" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Processing action</h4>
            </div>
            <div class="modal-progress-body">
                <div id="modal-progress-body-text">

                </div>
                <div class="progress">
                    <div id="modal-progress-bar" role="progressbar"
                         class="progress-bar progress-bar-striped active"
                         style="width: 0%"
                         aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
            <div id="modal-progress-footer" class="modal-footer">
            </div>
        </div>

    </div>
</div>