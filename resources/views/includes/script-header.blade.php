<script>
    window.routes = {};
    window.routes['song.delete'] = '{{ route('song.delete') }}';
    window.routes['song.play'] = '{{ route('song.play') }}';
    window.routes['mbot.queuesong'] = '{{ route('mbot.queuesong') }}';
    window.routes['playlist.list'] = '{{ route('playlist.list') }}';
    window.routes['playlist.ajaxList'] = '{{ route('playlist.ajaxList') }}';
    window.routes['playlist.view'] = '{{ route('playlist.view') }}';
    window.routes['playlist.delete'] = '{{ route('playlist.delete') }}';
    window.routes['playlist.play'] = '{{ route('playlist.play') }}';
    window.routes['playlist.publish'] = '{{ route('playlist.publish') }}';
    window.routes['playlist.depublish'] = '{{ route('playlist.depublish') }}';
    window.routes['playlist.songdelete'] = '{{ route('playlist.songdelete') }}';
    window.routes['lib.importPreview'] = '{{ route('lib.importPreview') }}';
    window.routes['lib.import'] = '{{ route('lib.import') }}';

    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    @if(Auth::check() && Auth::user()->hasCurrentMBot())
        T3MBotConnect.setCurrentBotId({{ Auth::user()->getCurrentMBot()->id }});
    @endif
    T3MBotConnect.ajaxInit();
</script>