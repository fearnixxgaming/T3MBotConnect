@extends('layouts.master')

@section('content')
    <div class="panel panel-default">
        <h2>Import from external URL</h2>
        <div>
            @php
                use \App\Http\Controllers\ImportController;
                $submitBtnEnabled = Auth::getSession()->has(ImportController::SKEY_IMPORT);
                $oldInfo = Auth::getSession()->get(ImportController::SKEY_IMPORT, null);
                $oldListUri = !is_null($oldInfo) ? $oldInfo->getImportUri() : '';
            @endphp
            <div class="input-group">
                <span class="input-group-addon">URL</span>
                <input id="import-uri" class="form-control" type="text" value="{{ $oldListUri }}">
                <span class="input-group-btn">
                    <button class="btn btn-info" onclick="T3MBotConnect.importUriPreview($('#import-uri').val(), 1)">
                        Check
                    </button>
                </span>
                <span class="input-group-btn">
                    <button id="import-submit"
                            class="btn {{ $submitBtnEnabled ? 'btn-success' : 'btn-disabled' }}"
                            onclick="{{ $submitBtnEnabled ? "T3MBotConnect.importUri($('#import-uri').val())" : '' }}">
                        Import
                    </button>
                </span>
                <script>
                    $('#import-uri').keydown(function () {
                        let submitBtn = $('#import-submit');
                        submitBtn.removeClass('btn-success');
                        submitBtn.add('btn-disabled');
                        submitBtn.attr('onClick', '');
                    })
                </script>
            </div>
        </div>
        <div id="preview-container">
            @if($submitBtnEnabled)
                <script>
                    T3MBotConnect.importUriPreview("{{ $oldListUri }}", {{ Request::get('page', 1) }});
                </script>
            @endif
        </div>
    </div>
@endsection