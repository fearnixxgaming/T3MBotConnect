<ul class="list-unstyled">
    @php
        use App\Http\Controllers\ImportController;
        /* @var $importInformation \App\Http\Preview\ImportInformation */
        /* @var $importPreviews \Illuminate\Contracts\Pagination\LengthAwarePaginator */
        /* @var $errors \Illuminate\Support\Collection*/
        $itemCounter = $startNum;
    @endphp
    @if($errors->any())
        @foreach($errors->all() as $error)
            <li>
                <div class="row">
                    <div class="col-md-12 center-block text-center teaser teaser-error">{{ $error }}</div>
                </div>
            </li>
        @endforeach
    @endif
    @if($importInformation->hasPlaylist())
        <div id="import-wants-playlist" class="alert alert-info">
            <div class="alert-body">
                <span class="glyphicon glyphicon-comment"></span>
                This URL may be imported as a playlist: <b>{{ $importInformation->getPlaylistName() }}</b>
            </div>
            <div class="alert-buttons">
                <button class="btn btn-primary" onclick="T3MBotConnect.setImportWantsPlaylist(true)">
                    Yes
                </button>
                <button class="btn btn-danger" onclick="T3MBotConnect.setImportWantsPlaylist(false)">
                    No
                </button>
            </div>
        </div>
    @endif
    @if($importPreviews->count() > 0)
        <li>
            {{ $importPreviews->links() }}
            <script>
                // Transmute pagination to use AJAX when JS is enabled.
                $(document).ready(function() {
                    $(document).on('click', '.pagination a', function (e) {
                        e.preventDefault();
                        T3MBotConnect.importUriPreview($('#import-uri').val(), $(this).attr('href').split('page=')[1]);
                    });
                });
            </script>
        </li>
        @foreach($importPreviews as $preview)
            @php
                /* @var $preview App\Http\Preview\ImportPreview */
                $itemCounter++;
            @endphp
            <li>
                <div class="teaser">
                    @if(!is_null($preview->getOriginalUrl()))
                        <h3 class="teaser-title">
                            {{ $itemCounter }}:
                            <a href="{{ $preview->getOriginalUrl() }}" rel="noreferrer,noopener" target="_blank">
                                {{ $preview->getTeaserTitle() }}
                            </a>
                        </h3>
                    @else
                        <h3 class="teaser-title">
                            {{ $itemCounter }}:
                            {{ $preview->getTeaserTitle() }}
                        </h3>
                    @endif
                    <div class="teaser-content row">
                        <div class="teaser-content-item col-md-2 col-xs-12">
                            <img src="{{ $preview->getTeaserImage() }}"/>
                        </div>
                        <div class="teaser-content-item teaser-description col-md-10 col-xs-12">
                            @if($preview->existsInDB())
                                <div class="center-block teaser-label">
                                    <div class="label label-danger">
                                        Already imported
                                    </div>
                                </div>
                            @endif
                            <div class="teaser-description-text">{{ $preview->getTeaserDescription() }}</div>
                        </div>
                    </div>
                </div>
            </li>
        @endforeach
    @endif
</ul>