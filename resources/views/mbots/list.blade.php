@extends('layouts.master')

@section('html-head')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/states.css') }}"/>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <form id="mbot-delete" class="form-invisible" method="POST" action="{{ route('mbot.delete') }}">
                        {{ csrf_field() }}
                        <input id="mbot-delete-id" type="hidden" name="mbot_id" value="0">
                    </form>
                    <table class="table table-unstyled">
                        <tr>
                            <th>Name</th>
                            <th>State</th>
                            @if(Auth::check() && Auth::user()->hasPermission(\App\Permission::BOTS_EDIT))
                                <th>Actions</th>
                            @endif
                        </tr>
                        @foreach($musicbots as $musicbot)
                            @php
                                $current_id = $musicbot->id;
                            @endphp
                            <tr>
                                <td class="selectable" onclick="location.href = '{{ URL::to('/musicbot/'.$musicbot->id) }}'">
                                    {{ $musicbot->display_name }}
                                </td>
                                <td class="table-cell-middle">
                                    <span class="label label-middle {{ $musicbot->getLockStateStr() }}"> {{ $musicbot->getLockState() !== 0 ? 'Locked' : 'Unlocked' }}</span>
                                </td>
                                @if(Auth::check() && Auth::user()->hasPermission(\App\Permission::BOTS_EDIT))
                                    <td>
                                        <div class="btn btn-danger btn-small" onclick="$('#mbot-delete-id').val({{ $current_id }}); $('#mbot-delete').submit();">Delete</div>
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                    </table>
                    {{ $musicbots->links() }}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 panel-default">
                <a class="link-unstyled" href="{{ URL::to('/musicbots/new') }}">
                    <span class="btn btn-success btn-small">
                        Add new
                    </span>
                </a>
            </div>
        </div>
    </div>
@endsection
