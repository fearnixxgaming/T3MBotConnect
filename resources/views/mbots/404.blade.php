@extends('layouts.master')

@section('content')
    <h1>Oh snap!</h1>
    <h2>404</h2>
    <p>The bot with id '{{ $mbot_id }}' could not be found!</p>
@endsection