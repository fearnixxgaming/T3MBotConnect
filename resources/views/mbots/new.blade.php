@extends('layouts.master')

@section('content')
    @php
        $testresult = Session::get('test');
        $result = Session::get('result');
    @endphp
    <div class="panel panel-default">
        <div class="row">
            <div class="col-md-6 col-xs-12">
                <h3>Add a new MusicBot endpoint</h3>
                <form id="mbot-create" class="form-horizontal" method="POST" action="{{ route('mbot.new') }}">
                    {{ csrf_field() }}
                    <input id="mbot-create-mode" name="mode" type="hidden" value="TEST">
                    @if($testresult === true || $result === true)
                        <div class="alert alert-success">
                            <ul>
                                @if($testresult === true)
                                    <li>Connection successful!</li>
                                @endif
                                @if($result === true)
                                    <li>Bot registered.</li>
                                @endif
                            </ul>
                        </div>
                    @endif
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="input-group">
                        <span class="input-group-addon">Display name:</span>
                        <input type="text" name="displayname" class="form-control" placeholder="MyBot" value="{{ old('displayname') }}">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Connection:</span>
                        <input type="text" name="url" class="form-control" placeholder="URL" style="width: available" value="{{ old('url') }}">
                        <span class="input-group-addon">:</span>
                        <input type="number" name="port" class="form-control" placeholder="Port" max="65536" min="1000" value="{{ old('port') }}">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Admin password:</span>
                        <input type="password" name="adminpass" class="form-control" value="{{ old('adminpass') }}">
                    </div>
                </form>
            </div>
            <div class="col-md-12">
                <script>
                    function submit_create_form(mode) {
                        $('#mbot-create-mode').val(mode);
                        $('#mbot-create').submit();
                    }
                </script>
                <span class="btn btn-primary" onclick="submit_create_form('TEST')">
                    Test connection
                </span>
                @if($testresult === true)
                    <span class="btn btn-success" onclick="submit_create_form('CREATE')">
                        Register
                    </span>
                @else
                    <span class="btn btn-default" disabled="true">
                        Register
                    </span>
                @endif
            </div>
        </div>
    </div>
@endsection