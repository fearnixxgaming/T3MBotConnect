@extends('layouts.master')

@section('sidebar-dynamic')
    <ul class="list-unstyled sidebar-list">
    </ul>
@endsection

@section('content-title')
    <div>
        <a class="link-unstyled" href="{{ route('mbot.show', $musicBot->id) }}">
            Bot: {{ $musicBot->display_name }}
        </a>
    </div>
@endsection

@section('content')
    @php
        use App\MusicBotLock;
        use App\Permission;
        $lockstate = $musicBot->getLockState();
        $controls_locked = !Auth::user()->hasPermission(Permission::BOTS_CONTROL) || ($musicBot->getLockState() > MusicBotLock::STATES['SHARED'] && !Auth::user()->isAdmin());

    @endphp
    <div class="container">
        <script>
            $(document).ready(function () {
                $('.match-height').matchHeight();
            })
        </script>
        <div class="col-md-5 col-xs-12 panel panel-default match-height">
            <h4>General information</h4>
            @if(Auth::user()->isAdmin())
                <span class="label label-danger">
                Admin
            </span>
            @endif
            <div class="table-responsive">
                <table class="table table-unstyled">
                    <tr>
                        <td>
                            State:
                        </td>
                        <td>
                            @if($lockstate === MusicBotLock::STATES['FREE'])
                                <form id="mbot-lock" class="form-invisible" method="POST" action="{{ route('mbot.lock') }}">
                                    {{ csrf_field() }}
                                    <input type="hidden" value="{{ $musicBot->id }}" name="mbot_id">
                                </form>
                                <span class="label label-success">
                                Available
                            </span>
                                <span class="label label-warning label-selectable" onclick="$('#mbot-lock').submit();">
                                lock
                            </span>
                            @endif
                            @if($lockstate === MusicBotLock::STATES['OWNED'])
                                <form id="mbot-lock" class="form-invisible" method="POST" action="{{ route('mbot.unlock') }}">
                                    {{ csrf_field() }}
                                    <input type="hidden" value="{{ $musicBot->id }}" name="mbot_id">
                                </form>
                                <span class="label label-info">
                                Restricted
                            </span>
                                <span class="label label-warning label-selectable" onclick="$('#mbot-lock').submit();">
                                unlock
                            </span>
                            @endif
                            @if($lockstate === MusicBotLock::STATES['SHARED'])
                                <span class="label label-info">
                                Shared
                            </span>
                            @endif
                            @if($lockstate === MusicBotLock::STATES['LOCKED'])
                                <form id="mbot-lock" class="form-invisible" method="POST" action="{{ route('mbot.requestlock') }}">
                                    {{ csrf_field() }}
                                    <input type="hidden" value="{{ $musicBot->id }}" name="mbot_id">
                                </form>
                                <span class="label label-warning">
                                Unavailable
                            </span>
                                <span class="label label-info label-selectable" onclick="$('#mbot-lock').submit();">
                                request access
                            </span>
                            @endif
                            @if($lockstate === MusicBotLock::STATES['OFFLINE'])
                                <span class="label label-danger">
                                Offline
                            </span>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>Volume:</td>
                        <td>{{ $info['volume'] }}</td>
                    </tr>
                    <tr>
                        <td>Song name:</td>
                        @if(!is_null($info['playing']))
                            <td>{{ $info['playing'] }}</td>
                        @else
                            <td></td>
                        @endif
                    </tr>
                    <tr>
                        <td>Length:</td>
                        @if(!is_null($info['length']))
                            <td>{{ gmdate('H:i:s', $info['length']) }}</td>
                        @else
                            <td></td>
                        @endif
                    </tr>
                </table>
            </div>
        </div>
        @if(!$controls_locked)
            <div class="col-md-7 col-xs-12 panel panel-default match-height">
                <h4>Control</h4>
                @include('forms.mbots.control')
            </div>
        @endif
        @if($share_requests->isNotEmpty())
            <div class="col-md-6 col-xs-12 panel panel-default">
                <h4>Share-Requests</h4>
                <div class="table-responsive">
                    <form id="form-share-lock" class="form-invisible" method="POST" action="{{ route('mbot.sharelock') }}">
                        {{ csrf_field() }}
                        <input id="share-req-id" type="hidden" value="0" name="share_id">
                        <script>
                            function grantShare(share_id) {
                                $('#share-req-id').val(share_id);
                                $('#form-share-lock').submit();
                            }
                        </script>
                    </form>
                    <table class="table-unstyled">
                        <tr>
                            <th>
                                User
                            </th>
                            <th>
                                Action
                            </th>
                        </tr>
                        @foreach($share_requests as $share_request)
                            <td>
                                {{ App\User::whereId($share_request->user_id)->get()->first()->name }}
                            </td>
                            <td>
                                <span class="label label-success label-selectable" onclick="grantShare({{ $share_request->id }})">Grant</span>
                                <span class="label label-warning">Ignore</span>
                            </td>
                        @endforeach
                    </table>
                </div>
            </div>
        @endif
        @if($shares_active->isNotEmpty())
            <div class="col-md-6 col-xs-12 panel panel-default">
                <h4>Active shares</h4>
                <div class="table-responsive">
                    <form id="form-revoke-share" class="form-invisible" method="POST" action="{{ route('mbot.unsharelock') }}">
                        {{ csrf_field() }}
                        <input id="share-lock-id" type="hidden" value="0" name="lock_id">
                        <input type="hidden" name="mbot_id" value="{{ $musicBot->id }}">
                        <script>
                            function revokeShare(lock_id) {
                                $('#share-lock-id').val(lock_id);
                                $('#form-revoke-share').submit();
                            }
                        </script>
                    </form>
                    <table class="table-unstyled">
                        <tr>
                            <th>
                                User
                            </th>
                            <th>
                                Action
                            </th>
                        </tr>
                        @foreach($shares_active as $share)
                            <td>
                                {{ App\User::whereId($share->user_id)->get()->first()->name }}
                            </td>
                            <td>
                                <span class="label label-danger label-selectable" onclick="revokeShare({{ $share->id }})">Revoke</span>
                            </td>
                        @endforeach
                    </table>
                </div>
            </div>
        @endif
        <div class="col-md-12 col-xs-12 panel panel-default match-height">
            <h4>Queue ({{ $musicBot->getQueueCount() }})</h4>
            <div class="table-responsive">
                <table class="table">
                    @foreach($musicBot->getQueue() as $item)
                        <tr>
                            <td>
                                {{ $item }}
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection