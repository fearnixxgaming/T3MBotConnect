<div class="col-md-12 col-xs-12">
    <div class="row">
        <form id="control-prev" class="form-invisible" method="POST" action="{{ route('mbot.prev') }}">
            <input type="hidden" value="{{ $musicBot->id }}" name="mbot_id">
            {{ csrf_field() }}
        </form>
        <form id="control-stop" class="form-invisible" method="POST" action="{{ route('mbot.stop') }}">
            {{ csrf_field() }}
            <input type="hidden" value="{{ $musicBot->id }}" name="mbot_id">
        </form>
        <form id="control-next" class="form-invisible" method="POST" action="{{ route('mbot.next') }}">
            {{ csrf_field() }}
            <input type="hidden" value="{{ $musicBot->id }}" name="mbot_id">
        </form>
        <form id="control-mute" class="form-invisible" method="POST" action="{{ route('mbot.mute') }}">
            {{ csrf_field() }}
            <input type="hidden" value="{{ $musicBot->id }}" name="mbot_id">
        </form>
        <div class="panel panel-control">
            <span class="selectable control glyphicon glyphicon-fast-backward" onclick="$('#control-prev').submit();"></span>
            <span class="selectable control glyphicon glyphicon-stop" onclick="$('#control-stop').submit();"></span>
            <span class="selectable control glyphicon glyphicon-fast-forward" onclick="$('#control-next').submit();"></span>
            <span class="selectable control glyphicon glyphicon-volume-off" onclick="$('#control-mute').submit();"></span>
            <input type="text" id="volume-slider"
                   data-slider-id="volume-slider"
                   data-slider-min="0"
                   data-slider-max="100"
                   data-slider-step="2.5"
                   data-slider-value="{{ $musicBot->getVolume() }}">
            <span id="volume-slider-save" style="display: none" onclick="changeVolume()"
                  class="selectable control glyphicon glyphicon-adjust"></span>
            <script>
                let volSlider = $('#volume-slider');
                volSlider.slider({
                    formatter: function (value) {
                        return 'Volume: ' + value + '%';
                    }
                });
                volSlider.on('slideStop', function (event) {
                    $('#volume-slider-save').css({'display': ''});
                });

                function changeVolume() {
                    $.post("{{ route('mbot.volume') }}", {
                        'mbot_id': "{{ $musicBot->id }}",
                        'volume' : volSlider.slider('getValue')
                    });
                    $('#volume-slider-save').css({'display': 'none'});
                }
            </script>
        </div>
    </div>
    <div class="row">
        <form class="form-inline" method="POST" action="{{ route('mbot.play') }}">
            {{ csrf_field() }}
            <input type="hidden" value="{{ $musicBot->id }}" name="mbot_id">
            <script>
                function setPlayType(type) {
                    $('#playtype').val(type);
                    $('#dropdown-playtype').html(type);
                }
            </script>
            <div class="col-md-12 col-xs-12">
                <div class="row{{ $errors->has('url') ? ' has-error' : '' }}">
                    <div class="dropdown dropdown-inline">
                        <input type="hidden" name="playtype" id="playtype" value="URL">
                        <a class="btn btn-default dropdown-toggle" id="dropdown-playtype" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            URL
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dropdown-playtype">
                            <li><a class="dropdown-item" href="#" onclick="setPlayType('URL'); return false;">URL</a></li>
                            <li><a class="dropdown-item" href="#" onclick="setPlayType('FILE'); return false;">File</a></li>
                        </ul>
                    </div>
                    <input type="text" name="url" class="form-control">
                    <button type="submit" class="btn btn-primary">Play</button>
                    <span class="help-block">
                        <strong>{{ $errors->first('url') }}</strong>
                    </span>
                </div>
            </div>
        </form>
        <form id="control-channel" class="form-inline" method="POST" action="{{ route('mbot.channel') }}">
            {{ csrf_field() }}
            <input type="hidden" value="{{ $musicBot->id }}" name="mbot_id">
            <div class="input-group">
                <span class="input-group-addon">ChannelID</span>
                <input id="control-channel-id" class="form-control" type="number" name="channel_id">
                <span class="input-group-addon btn btn-default" onclick="$('#control-channel').submit();">Send</span>
                @if(env('TS3DB_ENABLE') === true)
                    <span class="input-group-addon btn btn-info" onclick="openChannels()">Select</span>
                @endif
            </div>

            <!-- Modal Dialog "Show channel" -->
            <div id="view-channel-modal-container" class="row">
            </div>
            <script>
                function openChannels() {
                    $('#view-channel-modal-container').load("{{ route('channel.view') }}");
                }
                $(document).ajaxComplete(function (event, xhr, settings) {
                    console.log(event, xhr, settings);
                    if (settings.url === "{{ route('channel.view') }}") {
                        $('#view-channel-modal').modal();
                    }
                })
            </script>
        </form>
    </div>
</div>