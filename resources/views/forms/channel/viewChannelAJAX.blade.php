@if(env('TS3DB_ENABLE') === true)
    <!-- Modal Dialog "View channels" -->
    <!-- Updated using AJAX | Placed in "#view-channel-modal-container" -->
    <div id="view-channel-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- modal header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3>Channels</h3>
                </div>
                <!-- modal body -->
                <div class="modal-body">
                    <ul class="list-unstyled">
                        @foreach($channels as $channel)
                            <li incr_order="{{ $channel->incr_order }}"
                                channel_id="{{ $channel->channel_id }}"
                                parent_id="{{ $channel->channel_parent_id }}"
                                order="{{ $channel->order }}"
                                class="selectable"
                                onclick="sendChannel({{ $channel->channel_id  }})">
                                {{ $channel->name }}
                            </li>
                        @endforeach
                    </ul>
                    <script>
                        function sendChannel(channel_id) {
                            $('#control-channel-id').val(channel_id);
                            $('#view-channel-modal').modal('hide');
                        }
                    </script>
                </div>
                <!-- modal footer -->
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!-- END Model Dialog -->
@else
    <div class="alert alert-danger">
        TS3 integration is disabled!
    </div>
@endif