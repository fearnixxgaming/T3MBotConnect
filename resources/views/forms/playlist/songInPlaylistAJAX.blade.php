<!-- Modal Dialog "Add to playlist" -->
<!-- Updated using AJAX | Placed in "#add-to-playlist-modal-container" -->
<div id="add-to-playlist-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- modal header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3>Add to playlist</h3>
            </div>
            <!-- modal body -->
            <div class="modal-body">
                <h4>Song title:</h4>
                <form id="add-to-playlist-form" method="POST" action="{{ route('song.songInPlaylist') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="song_id" value="{{ $song->id }}">
                    <hr/>
                    <h4>Your playlists</h4>
                    <ul id="checked-playlists" class="list-unstyled">
                        @foreach($playlists['own'] as $playlist)
                            <div class="checkbox">
                                <label><input type="checkbox" name="playlist-{{ $playlist->id }}" {{ $playlist->containsSong($song->id) ? 'checked' : '' }}>{{ $playlist->display_name }}</label>
                            </div>
                        @endforeach
                    </ul>
                    @if(Auth::user()->hasPermission(\App\Permission::PLAYLIST_CREATE_PRIVATE))
                        <div class="input-group">
                            <span class="input-group-addon">New:</span>
                            <input class="form-control" id="new-playlist-name" placeholder="new-playlist">
                            <span class="input-group-addon btn btn-info" onclick="addPlaylist($('#new-playlist-name').val())">Add</span>
                        </div>
                    @endif
                    <hr/>
                </form>
            </div>
            <!-- modal footer -->
            <div class="modal-footer">
                <div class="btn btn-success" onclick="$('#add-to-playlist-form').submit()">
                    Save
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END Model Dialog -->