@php
    /* @var $playlist \App\Playlist */
    $modificationAllowed = $playlist->user_id == Auth::user()->id || Auth::user()->hasPermission(\App\Permission::PLAYLIST_MODIFY_UNOWNED);
@endphp
<!-- Modal Dialog "Add to playlist" -->
<!-- Updated using AJAX | Placed in "#add-to-playlist-modal-container" -->
<div id="view-playlist-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- modal header -->
            <div class="modal-header">
                <button id="view-playlist-modal-close" type="button" class="close" data-dismiss="modal">&times;</button>
                <h3>Playlist: {{ $playlist->display_name }}</h3>
                @if($playlist->is_public && $modificationAllowed)
                    <span class="label label-selectable label-info"
                          data-toggle="tooltip" title="Make private"
                          onclick="T3MBotConnect.depublishPlaylist({{ $playlist->id }})">public</span>
                @elseif($playlist->is_public && !$modificationAllowed)
                    <span class="label label-info">public</span>
                @elseif(Auth::user()->hasPermission(\App\Permission::PLAYLIST_CREATE_PUBLIC))
                    <span class="label label-selectable label-success"
                          data-toggle="tooltip" title="Make public"
                          onclick="T3MBotConnect.publishPlaylist({{ $playlist->id }})">private</span>
                @else
                    <span class="label label-success">private</span>
                @endif
                @if(Auth::user()->id !== $playlist->user_id)
                    <span class="label label-warning">unowned</span>
                @endif
                @if(!is_null($playlist->external_id))
                    <span class="label label-danger">imported</span>
                @endif
            </div>
            <!-- modal body -->
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <td>Title</td>
                            <td>Action</td>
                        </tr>
                        @foreach($songs as $song)
                            <tr id="viewed-song-{{ $song->id }}">
                                <td>
                                    <span>{{ $song->title }}</span>
                                </td>
                                <td>
                                    @if(Auth::user()->hasCurrentMBot())
                                        <span class="glyphicon glyphicon-play selectable"
                                              onclick="T3MBotConnect.instantPlaySong({{ $song->id }})"></span>
                                    @endif
                                    @if($modificationAllowed)
                                        <span class="label label-danger label-selectable"
                                              onclick="T3MBotConnect.deleteSongFromList('{{ $song->id }}', '{{ $playlist->id }}')">Remove</span>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
                <hr/>
            </div>
            <!-- modal footer -->
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<!-- END Model Dialog -->