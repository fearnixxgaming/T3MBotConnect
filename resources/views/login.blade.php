@extends('layouts.master')

@section('content')
    <div>
        <h1>Login</h1>
        @include('forms.login')
    </div>
@endsection