@php
    /* @var $playlists \Illuminate\Support\Collection */
    $hasPublic = $playlists['public']->count() > 0;
    $hasOther = $playlists['other']->count() > 0;
@endphp
<div class="col-md-12 col-xs-12">
    <ul class="nav nav-tabs">
        @if($hasPublic)
            <li id="playlist-selector-public" class="active playlist-selector"
                onclick="T3MBotConnect.showPlaylistTab('public')">
                <a href="#">Public</a>
            </li>
        @endif
        @if(Auth::user()->hasPermission(\App\Permission::PLAYLIST_CREATE_PRIVATE))
            <li id="playlist-selector-own" class="{{ $hasPublic ? '' : 'active' }} playlist-selector"
                onclick="T3MBotConnect.showPlaylistTab('own')">
                <a href="#">Private</a>
            </li>
        @endif
        @if(Auth::user()->hasPermission(\App\Permission::PLAYLIST_MODIFY_UNOWNED) && $hasOther)
            <li id="playlist-selector-other" class="playlist-selector"
                onclick="T3MBotConnect.showPlaylistTab('other')">
                <a href="#">Moderation</a>
            </li>
        @endif
    </ul>
    <div id="playlists-public" class="playlist-tab {{ $hasPublic ? '' : 'hidden' }}">
        @if(Auth::user()->hasPermission(\App\Permission::PLAYLIST_VIEW))
            <h4>Public Play lists</h4>
            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                    @foreach($playlists['public'] as $playlist)
                        <tr id="playlist-{{ $playlist->id }}">
                            <td>
                                <span onclick="T3MBotConnect.openPlaylist({{ $playlist->id }})">
                                    {{ $playlist->getPublicDisplayName() }}
                                </span>
                            </td>
                            <td>
                                @if(Auth::user()->hasCurrentMBot())
                                    <span class="glyphicon glyphicon-play"
                                          onclick="T3MBotConnect.playPlaylist({{ $playlist->id }})"></span>
                                @endif
                                @if(Auth::user()->hasPermission(\App\Permission::PLAYLIST_MODIFY_UNOWNED))
                                    <span class="label label-warning label-selectable"
                                          onclick="T3MBotConnect.depublishPlaylist({{ $playlist->id }})">depublish</span>
                                    <span class="label label-danger label-selectable"
                                          onclick="T3MBotConnect.deletePlaylist({{ $playlist->id }})">&times;</span>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        @endif
    </div>
    <div id="playlists-own" class="playlist-tab {{ $hasPublic ? 'hidden' : '' }}">
        @if(Auth::user()->hasPermission(\App\Permission::PLAYLIST_CREATE_PRIVATE))
            <h4>Your playlists</h4>
            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                    @foreach($playlists['own'] as $playlist)
                        <tr id="playlist-{{ $playlist->id }}">
                            <td>
                                    <span onclick="T3MBotConnect.openPlaylist({{ $playlist->id }})">
                                        {{ $playlist->display_name }}
                                    </span>
                            </td>
                            <td>
                                @if(Auth::user()->hasPermission(\App\Permission::PLAYLIST_CREATE_PUBLIC))
                                    <span class="label {{ $playlist->is_public ? 'label-warning' : 'label-success'}}">
                                           {{ $playlist->is_public ? 'Public' : 'Private'}}
                                        </span>
                                @endif
                                @if(Auth::user()->hasCurrentMBot())
                                    <span class="glyphicon glyphicon-play" onclick="T3MBotConnect.playPlaylist({{ $playlist->id }})"></span>
                                @endif
                                <span class="label label-danger label-selectable" onclick="T3MBotConnect.deletePlaylist({{ $playlist->id }})">&times; </span>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <div class="panel panel-default">
                <a class="btn btn-info" href="{{ route('playlists.import') }}">Import</a>
            </div>
        @endif
    </div>
    <div id="playlists-other" class="playlist-tab hidden">
        @if(Auth::user()->hasPermission(\App\Permission::PLAYLIST_MODIFY_UNOWNED))
            <h4>Other playlists</h4>
            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <th>Name</th>
                        @if(Auth::user()->hasCurrentMBot())
                            <th>Action</th>
                        @endif
                    </tr>
                    @foreach($playlists['other'] as $playlist)
                        <tr id="playlist-{{ $playlist->id }}">
                            <td>
                                    <span onclick="T3MBotConnect.openPlaylist({{ $playlist->id }})">
                                        {{ $playlist->display_name }}
                                    </span>
                            </td>
                            <td>
                                @if(Auth::user()->hasPermission(\App\Permission::PLAYLIST_CREATE_PUBLIC))
                                    <span class="label {{ $playlist->is_public ? 'label-warning' : 'label-success'}}">
                                                {{ $playlist->is_public ? 'Public' : 'Private'}}
                                            </span>
                                @endif
                                @if(Auth::user()->hasCurrentMBot())
                                    <span class="glyphicon glyphicon-play" onclick="T3MBotConnect.playPlaylist({{ $playlist->id }})"></span>
                                @endif
                                <span class="label label-danger label-selectable" onclick="T3MBotConnect.deletePlaylist({{ $playlist->id }})">&times; </span>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        @endif
    </div>
</div>