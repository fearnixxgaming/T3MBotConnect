@extends('layouts.master')

@section('content')
    <div class="panel panel-default">
        <div id="playlists-body" class="row">
            @include('listings.playlists-body')
        </div>
        <!-- Modal Dialog "Show playlist" -->
        <div id="view-playlist-container">
        </div>
    </div>
@endsection