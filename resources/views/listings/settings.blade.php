@extends('layouts.master')

@section('content')
    <div class="table-responsive">
        <table class="table">
            <tr>
                <th>Identifier</th>
                <th>Value (240)</th>
                <th>Default</th>
            </tr>
            <script>
                function saveSetting(ident) {
                    let value = $('#setting-' + ident + '-value').val();

                    $.post("{{ route('setting.view') }}", {
                        'ident': ident,
                        'value': value
                    })
                }
                function showSave(ident) {
                    $('#setting-' + ident + '-save').attr('style', '');
                }
            </script>
            @foreach($settings as $setting)
                <tr>
                    <td>{{ $setting->ident }}</td>
                    <td>
                        <textarea id="setting-{{ $setting->ident }}-value"
                                  class="setting-value"
                                  maxlength="450"
                                  onchange="showSave('{{ $setting->ident }}')">{{ $setting->value }}</textarea>
                        <span id="setting-{{ $setting->ident }}-save"
                              class="glyphicon glyphicon-transfer"
                              style="display: none"
                              onclick="saveSetting('{{ $setting->ident }}')"></span>
                    </td>
                    <td>{{ $setting->default }}</td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection