@extends('layouts.master')

@section('content')
    <div class="panel panel-default">
        <div class="row">
            @if(Auth::user()->hasPermission(\App\Permission::SONG_REGISTER))
                <div class="col-md-6 col-xs-12">
                    <h4>Register a song</h4>
                    <form class="form" method="post" action="{{ route('song.register') }}">
                        {{ csrf_field() }}
                        <div class="input-group{{ $errors->any('title') ? ' has-error' : '' }}">
                            <span class="input-group-addon">Title</span>
                            <input name="title" type="text" class="form-control" value="{{ old('title') }}">
                        </div>
                        @if($errors->has('title'))
                            <div class="alert-danger">{{ $errors->first('title') }}</div>
                        @endif
                        <div class="input-group{{ $errors->any('uri') ? ' has-error' : ''}}">
                            <span class="input-group-addon">URI</span>
                            <input name="uri" type="url" class="form-control" value="{{ old('uri') }}">
                        </div>
                        @if($errors->has('uri'))
                            <div class="alert-danger">{{ $errors->first('uri') }}</div>
                        @endif
                        <input type="submit" class="btn btn-default" value="Register">
                    </form>
                </div>
            @endif
            <div class="col-md-6 col-xs-12">
                <h4>Registered songs</h4>
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th>Name</th>
                            @if(Auth::user()->hasCurrentMBot())
                                <th>Action</th>
                            @endif
                        </tr>
                        @foreach($songs as $song)
                            <tr id="song-{{ $song->id }}">
                                <td>
                                    @if(Auth::user()->hasPermission(\App\Permission::SONG_REGISTER))
                                        <span class="btn btn-small btn-danger"
                                              onclick="T3MBotConnect.deleteSong({{ $song->id }})">&times;</span>
                                    @endif
                                    <span class="selectable" data-toggle="modal" onclick="selectPlaylistsFor({{ $song->id }})">
                                        {{ $song->title }}
                                    </span>
                                </td>
                                @if(Auth::user()->hasCurrentMBot())
                                    <td>
                                        <span class="glyphicon glyphicon-play selectable"
                                              onclick="T3MBotConnect.instantPlaySong('{{ $song->id }}')"
                                              data-toggle="tooltip" title="Play"></span>
                                        <span class="glyphicon glyphicon-plus selectable"
                                              onclick="T3MBotConnect.queueSong('{{ $song->uri }}')"
                                              data-toggle="tooltip" title="Add to queue"></span>
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
        <!-- Modal Dialog "Add to playlist" -->
        <div id="add-to-playlist-modal-container">
        </div>
        <script>
            function addPlaylist(name) {
                let newCheckBox = $('<input>', {
                    type: 'checkbox',
                    name: 'playlist-new-' + name,
                    checked: true
                });
                let newLabel = $('<label>').append(newCheckBox, name);
                let newBox = $('<div/>', {
                    class: 'checkbox'
                }).append(newLabel);
                $('#checked-playlists').append(newBox);
            }

            function selectPlaylistsFor(song_id) {
                $('#add-to-playlist-modal-container').load("{{ route('song.AJAX.songInPlaylist') }}", {'song_id': song_id});
            }

            $(document).ajaxComplete(function (event, xhr, settings) {

                if (settings.url === "{{ route('song.AJAX.songInPlaylist') }}") {
                    $('#add-to-playlist-modal').modal();

                } else if (settings.url === "{{ route('song.delete') }}") {
                    let song_id = settings.data.split('=')[1];
                    $('#song-' + song_id).remove();

                }
            });
        </script>
        <!-- END Model Dialog -->
@endsection