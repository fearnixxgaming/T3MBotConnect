@extends('layouts.master')

@section('content')
    <div class="panel panel-default">
        <div class="row">
            <div class="table-responsive">
                <form id="user-delete" class="form-invisible" method="POST" action="{{ route('user.delete') }}">
                    {{ csrf_field() }}
                    <input id="user-delete-id" type="hidden" name="user_id" value="0">
                </form>
                <table class="table table-unstyled">
                    <tr>
                        <th>Name</th>
                        <th>E-Mail</th>
                        @if(Auth::user()->hasPermission(\App\Permission::USERS_EDIT))
                            <th>Actions</th>
                            <form id="form-activate-user" method="POST" action="{{ route('user.activate') }}">
                                {{ csrf_field() }}
                                <input id="form-activate-user-id" type="hidden" name="user_id" value="0">
                                <input type="hidden" name="active" value="true">
                                <script>
                                    function activateUser(user_id) {
                                        if (user_id !== null) {
                                            $('#form-activate-user-id').val(user_id);
                                            $('#form-activate-user').submit();
                                        }
                                    }
                                </script>
                            </form>
                        @endif
                    </tr>
                    @foreach($users as $user)
                        @php
                            $current_id = $user->id;
                        @endphp
                        <tr class="selectable" onclick="location.href = '{{ route('user.show', $user->id) }}'">
                            <td>
                                @if(!$user->activated)
                                    <span class="label label-warning label-selectable"
                                          data-toggle="tooltip"
                                          title="Click to activate"
                                          onclick="activateUser({{ Auth::user()->hasPermission(\App\Permission::USERS_ASSIGN) ? $user->id : 'null'}}); event.stopPropagation();">inactive</span>
                                @endif
                                @if($user->banned)
                                    <span class="label label-danger">banned</span>
                                @endif
                                <span>{{ $user->name }}</span>
                            </td>
                            <td><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></td>
                            @if(true)
                                <td>
                                    <button class="btn btn-danger btn-small" onclick="$('#user-delete-id').val({{ $current_id }}); $('#user-delete').submit(); event.stopPropagation()">Delete</button>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 panel-default">
                <a class="link-unstyled" href="{{ route('user.register') }}">
                    <span class="btn btn-success btn-small">
                        Add new
                    </span>
                </a>
            </div>
        </div>
    </div>
@endsection