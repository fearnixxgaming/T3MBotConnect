@extends('layouts.master')

@section('content')
    <div class="container panel panel-default">
        <h4>Create new user</h4>
        <div class="row">
            <div class="col-md-6 col-xs-12">
                <form method="POST" action="{{ route('user.new') }}">
                    {{ csrf_field() }}
                    <div class="input-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <span class="input-group-addon">Name</span>
                        <input value="{{ old('name') }}" class="form-control" type="text" name="name" placeholder="SomeUserName" required autofocus>
                    </div>
                    <div class="input-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <span class="input-group-addon">E-Mail</span>
                        <input value="{{ old('email') }}" class="form-control" type="text" name="email" placeholder="user@example.com" required>
                    </div>
                    <div class="input-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <span class="input-group-addon">Password</span>
                        <input value="{{ str_random(16) }}" class="form-control" type="text" name="password" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection