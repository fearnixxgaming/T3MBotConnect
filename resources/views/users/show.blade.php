@extends('layouts.master')

@section('content')
    <div class="panel panel-default">
        <div class="row">
            <div class="col-md-5 col-xs-12">
                <h4>Data</h4>
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th>
                                State
                            </th>
                            <td>
                                <form id="form-activate-user" method="POST" action="{{ route('user.activate') }}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="user_id" value="{{ $user->id }}">
                                    <input type="hidden" name="active" value="{{ $user->activated ? 'false' : 'true' }}">
                                </form>
                                <form id="form-ban-user" method="POST" action="{{ route('user.ban') }}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="user_id" value="{{ $user->id }}">
                                    <input type="hidden" name="banned" value="{{ $user->banned ? 'false' : 'true' }}">
                                </form>
                                @if($user->activated)
                                    <span class="label label-success label-selectable"
                                          data-toggle="tooltip" title="Click to de-activate"
                                          onclick="$('#form-activate-user').submit()">
                                        Active
                                    </span>
                                @else
                                    <span class="label label-warning label-selectable"
                                          data-toggle="tooltip" title="Click to activate"
                                          onclick="$('#form-activate-user').submit()">
                                        Inactive
                                    </span>
                                @endif
                                @if($user->banned)
                                    <span class="label label-danger label-selectable"
                                          data-toggle="tooltip" title="Click to un-ban"
                                          onclick="$('#form-ban-user').submit()">
                                        Banned
                                    </span>
                                @else
                                    <span class="label label-success label-selectable"
                                          data-toggle="tooltip" title="Click to ban"
                                          onclick="$('#form-ban-user').submit()">
                                        Not banned
                                    </span>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Name
                            </th>
                            <td>
                                {{ $user->name }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                E-Mail-Address
                            </th>
                            <td>
                                {{ $user->email }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Created at
                            </th>
                            <td>
                                {{ $user->created_at }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Updated at
                            </th>
                            <td>
                                {{ $user->updated_at }}
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-md-5 col-xs-12">
                <h4>Groups</h4>
                @if($errors->hasAny('update_count'))
                  <div class="alert alert-success">
                     {{ 'Updated '.$errors->get('update_count')[0].' values.' }}
                  </div>
                @endif
                <form id="groups-for-user" method="POST" action="{{ route('user.edit.groups') }}">
                  {{ csrf_field() }}
                  <input type="hidden" name="user_id" value="{{ $user->id }}">
                    <div class="table-responsive">
                        <ul class="list-unstyled">
                         @foreach($groups as $group)
                                <li>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" {{ $user->isGroupMember($group->id) ? "checked=checked" : ""}}" name="group-{{ $group->id }}">{{ $group->display_name }}
                                            @if($group->isAdmin())
                                                <span class="label label-danger">admin</span>
                                            @endif
                                        </label>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                        <div class="btn btn-primary" onclick="$('#groups-for-user').submit()">
                          Save
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
