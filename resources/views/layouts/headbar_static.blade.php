@section('headbar')
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#app-navbar-collapse" aria-expanded="false">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="#">
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                @if(Auth::check() && Auth::user()->hasCurrentMBot())
                    <li>
                        @if(Auth::user()->getCurrentMBot()->getLockState() === \App\MusicBotLock::STATES['OWNED'])
                            <form id="mbot-lock-relock" class="form-invisible" method="POST"
                                  action="{{ route('mbot.relock') }}">
                                {{ csrf_field() }}
                                <input type="hidden" value="{{ Auth::user()->getCurrentMBot()->id }}" name="mbot_id">
                            </form>
                            <script>
                                function refreshLock() {
                                    $('#mbot-lock-relock').submit();
                                    return false;
                                }

                                @php
                                    $lock = \App\MusicBotLock::whereUserId(Auth::user()->id)->whereMbotId(Auth::user()->getCurrentMBot()->id)->get()->first();
                                    $cTime = Carbon\Carbon::now(config('app.timezone'));
                                    $lastRefresh = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $lock->refreshed_on, config('app.timezone'))
                                                    ->addMinutes(5);
                                    $difference = $lastRefresh->diffInSeconds($cTime) - 30;
                                    if ($difference < 0)
                                    {
                                        $difference = 0;
                                    }
                                @endphp
                                console.log('Auto-relock scheduled for {{ $difference }} seconds');
                                window.setTimeout(refreshLock, {{ $difference * 1000 }});
                            </script>
                        @endif
                        <a href="{{ route('mbot.show', Auth::user()->getCurrentMBot()->id) }}">
                            <div class="label label-primary">
                                {{ Auth::user()->getCurrentMBot()->display_name }}
                            </div>
                        </a>
                    </li>
                @endif
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @guest
                    <li><a href="{{ URL::to('/login') }}">Login</a></li>
                    <li><a href="{{ URL::to('/register') }}">Register</a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"
                           aria-haspopup="true">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{ URL::to('/') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                            @if(Config::get('app.debug', false))
                                <li>
                                    <a href="{{ route('debug.session.clear') }}">Reset session</a>
                                </li>
                            @endif
                        </ul>
                    </li>
                @endguest
            </ul>
        </div>

        @if($errors->has('error'))
            <div class="alert alert-danger">
                {{ $errors->first('error') }}
            </div>
        @endif
    </div>
@endsection