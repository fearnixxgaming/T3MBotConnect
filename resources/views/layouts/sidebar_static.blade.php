@section('sidebar')
    <div class="sidebar-header">
        <h1>{{ config('app.name', 'Test') }}</h1>
    </div>
    <hr/>
    <ul class="list-unstyled sidebar-list">
        <li class="sidebar-item selectable" onclick="location.href = '{{ route('root') }}'">Home</li>
        <li class="sidebar-item selectable" onclick="location.href = '{{ route('about') }}'">About</li>
        @if(Auth::check())
            @if(Auth::user()->hasPermission(\App\Permission::BOTS_VIEW))
                <li class="sidebar-item selectable" onclick="location.href = '{{ route('mbot.list') }}'">MusicBots</li>
            @endif
            @if(Auth::user()->hasPermission(\App\Permission::USERS_VIEW))
                <li class="sidebar-item selectable" onclick="location.href = '{{ route('user.list') }}'">Users</li>
            @endif
            @if(Auth::user()->hasPermission(\App\Permission::GROUPS_VIEW))
                <li class="sidebar-item selectable" onclick="location.href = '{{ route('group.list') }}'">Groups</li>
            @endif
            @if(Auth::user()->hasPermission(\App\Permission::SETTINGS_EDIT))
                <li class="sidebar-item selectable" onclick="location.href = '{{ route('setting.view') }}'">Settings</li>
            @endif
            @if(Auth::user()->hasPermission(\App\Permission::PLAYLIST_VIEW))
                <li class="sidebar-item selectable" onclick="location.href = '{{ route('playlist.list') }}'">Playlists</li>
            @endif
            @if(Auth::user()->hasPermission(\App\Permission::SONG_VIEW))
                <li class="sidebar-item selectable" onclick="location.href = '{{ route('song.list') }}'">Songs</li>
            @endif
                <li class="sidebar-item selectable disabled">Radios</li>
        @endif
    </ul>
    <hr/>
    @yield('sidebar-dynamic')
@endsection