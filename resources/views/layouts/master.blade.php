@include('layouts.sidebar_static')
@include('layouts.headbar_static')

<html>
    <head>

        <!-- Meta information -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Stylesheets -->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}"/>
        <link rel="stylesheet" type="text/css" href="{{ asset('css/master.css') }}"/>
        <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-slider.min.css') }}"/>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{ asset('js/jquery.matchHeight.js') }}"></script>
        <script src="{{ asset('js/bootstrap-slider.js') }}"></script>
        <script src="{{ asset('js/t3mbotconnect.js') }}"></script>

        <!-- Initializer -->
        @include('includes.script-header')

        @yield('html-head')
    </head>
    <body>
        <div class="wrapper">
            <nav id="sidebar">
                @yield('sidebar')
            </nav>

            <div id="content">
                <nav class="navbar navbar-default navbar-static-top">
                    @yield('headbar')
                </nav>
                <div class="container">
                    @yield('content')
                </div>
            </div>
        </div>

        <!-- Loading modal dialog -->
        @include('includes.modal-progress')
    </body>
</html>