const jQuery = require('jquery');

export class T3MBotConnect {

    constructor() {
        this.currentBotId = null;
        this.songsInQueue = [];
        this.songQueueDoneCount = 0;
        this.previewLoading = false;
        this.importWantsPlaylist = true;
        this.progressBar = null;
        this.progressModalDialog = null;
        this.progressText = null;
    }

    setCurrentBotId(id) {
        this.currentBotId = id;
    }

    checkHasMBot() {
        if (this.currentBotId !== null && (typeof this.currentBotId) === 'number') {
            return true;
        } else {
            console.log('No musicbot selected ... cant do this.');
            return false;
        }
    }

    activateProgressModal() {
        if (this.progressBar == null)
            this.progressBar = jQuery('#modal-progress-body');
        if (this.progressModalDialog == null)
            this.progressModalDialog = jQuery('#modal-progress');
        this.progressModalDialog.modal({backdrop: 'static', keyboard: false});
    }

    deactivateProgressModal() {
        $('#modal-progress-close').click();
    }

    setProgressValue(value) {
        if (this.progressBar == null)
            this.progressBar = jQuery('#modal-progress-bar');
        this.progressBar.attr('aria-valuenow', value);

        let percent = (this.progressBar.attr('aria-valuenow') / this.progressBar.attr('aria-valuemax')) * 100;
        this.progressBar.css('width',  percent + '%');

    }

    setProgressMaxValue(value) {
        if (this.progressBar == null)
            this.progressBar = jQuery('#modal-progress-bar');
        this.progressBar.attr('aria-valuemax', value);
    }

    setProgressText(value) {
        if (this.progressText == null)
            this.progressText = jQuery('#modal-progress-body-text');
        this.progressText.html(value);
    }

    queueSong(song_uri) {
        if (this.checkHasMBot()) {
            jQuery.post(window.routes['mbot.queuesong'], {
                mbot_id: this.currentBotId,
                song_uri: song_uri
            });
        }
    }

    get songQueue() {
        return this.songsInQueue;
    }

    set songQueue(queue) {
        this.songsInQueue = queue;
    }

    instantPlaySong(song_id) {
        if (this.checkHasMBot()) {
            jQuery.post(window.routes['song.play'], {
                mbot_id: this.currentBotId,
                song_id: song_id,
            })
        }
    }

    deleteSong(song_id) {
        jQuery.post(window.routes['song.delete'], {
            'song_id': song_id
        });
    }

    showPlaylistTab(type) {
        jQuery('.playlist-tab').each(function (index) {
            jQuery(this).addClass('hidden');
        });
        jQuery('.playlist-selector').each(function (index) {
            jQuery(this).removeClass('active');
        });

        jQuery('#playlist-selector-' + type).addClass('active');
        jQuery('#playlists-' + type).removeClass("hidden");
    }

    reloadPlaylists() {
        jQuery('#playlists-body').load(window.routes['playlist.ajaxList']);

        let modalClose = jQuery('#view-playlist-modal');
        if (modalClose) {
            modalClose.click();
        }
    }

    publishPlaylist(playlist_id) {
        jQuery.post(window.routes['playlist.publish'], {
            playlist_id: playlist_id
        });
    }

    depublishPlaylist(playlist_id) {
        jQuery.post(window.routes['playlist.depublish'], {
            playlist_id: playlist_id
        });
    }

    openPlaylist(playlist_id) {
        jQuery('#view-playlist-container')
            .load(window.routes['playlist.view'], {
                playlist_id: playlist_id
            });
    }

    deletePlaylist(playlist_id) {
        jQuery.post(window.routes['playlist.delete'], {
            playlist_id: playlist_id
        });
    }

    playPlaylist(playlist_id) {
        if (this.checkHasMBot()) {
            jQuery.post(window.routes['playlist.play'], {
                playlist_id: playlist_id,
                mbot_id: this.currentBotId
            });
        }
    }

    deleteSongFromList(song_id, playlist_id) {
        jQuery.post(window.routes['playlist.songdelete'], {
            playlist_id: playlist_id,
            song_id: song_id
        });
    }

    importUriPreview(uri, page) {
        if (this.previewLoading === true)
            return;

        let loadingLabel = jQuery('<div class="col-md-12 center-block text-center teaser teaser-loading">Loading preview ...</div>');
        let loadingContainer = jQuery('<div class="row"/>');
        loadingContainer.html(loadingLabel);

        let elem = jQuery('#preview-container');
        elem.html(loadingContainer);
        elem.load(window.routes['lib.importPreview'], {
            uri: uri,
            page: page ? page : 1
        });
    }

    importUri(uri) {
        let jThis = this;
        jQuery.post(window.routes['lib.import'], {
            uri: uri,
            create_playlist: jThis.importWantsPlaylist
        });
    }

    setImportWantsPlaylist(value) {
        this.importWantsPlaylist = value;
        let elem = $('#import-wants-playlist');
        if (elem) {
            elem.remove();
        }
    }

    ajaxInit() {
        let jThis = this;
        jQuery(document).ajaxComplete(function (event, xhr, settings) {

            if (settings.url === window.routes['playlist.view']) {
                // A playlist shall be viewed modally
                let modalDialog = jQuery('#view-playlist-modal');
                if (modalDialog) {
                    modalDialog.modal();
                } else {
                    console.log("No modal dialog to open.")
                }

            } else if (settings.url === window.routes['playlist.delete']) {
                // A playlist shall be deleted
                let playlist_id = settings.data.split('=')[1];
                console.log(playlist_id);

                let playListElement = jQuery('#playlist-' + playlist_id);
                if (playListElement) {
                    playListElement.remove();
                } else {
                    console.log("No playlist element to delete.");
                }

            } else if (settings.url === window.routes['playlist.play']) {
                // A playlist shall be played (added to queue)
                jThis.songQueueDoneCount = 0;
                jThis.songQueue = JSON.parse(xhr.responseText);
                if (jThis.songQueue.length > 0) {
                    jThis.queueSong(jThis.songQueue[0].uri, jThis.currentBotId);
                }
                jThis.setProgressMaxValue(jThis.songQueue.length);
                jThis.setProgressValue(0);
                jThis.activateProgressModal();

            } else if (settings.url === window.routes['mbot.queuesong']) {
                // A song shall be queued
                let prevURI = decodeURIComponent(settings.data.split('&')[1].split('=')[1]);
                let next = null;

                for (let i = 0; i < jThis.songQueue.length; i++) {
                    let song = jThis.songQueue[i];

                    console.log(song.uri, prevURI);
                    if (song.uri === prevURI) {
                        song.queued = true;
                        jThis.songQueueDoneCount++;
                        continue;
                    }

                    if (song.queued === false && next === null) {
                        next = song.uri;
                    }
                }

                jThis.setProgressValue(jThis.songQueueDoneCount);
                jThis.setProgressText('Queuing songs: ' + jThis.songQueueDoneCount + ' of ' + jThis.songQueue.length)

                if (next !== null) {
                    jThis.queueSong(next);
                }

            } else if (settings.url === window.routes['playlist.songdelete']) {
                // A song was deleted
                let song_id = settings.data.split('&')[1].split('=')[1];
                let song_elem = jQuery('#viewed-song-' + song_id);
                if (song_elem) {
                    song_elem.remove();
                } else {
                    console.log("No song element to remove.");
                }

            } else if (settings.url === window.routes['lib.importPreview']) {
                jThis.previewLoading = false;
                jThis.importWantsPlaylist = true;

            } else if (settings.url === window.routes['lib.import']
                || settings.url === window.routes['playlist.publish']
                || settings.url === window.routes['playlist.depublish']) {
                location.reload();
            }
        });
    }
}