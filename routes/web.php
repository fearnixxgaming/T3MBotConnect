<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;

use \App\Http\Controllers\DebugController;
use \App\Http\Controllers\DbSettingController;
use \App\Http\Controllers\UserController;
use \App\Http\Controllers\GroupController;

use \App\Http\Controllers\MusicBotController;
use \App\Http\Controllers\MusicBotLockController;
use \App\Http\Controllers\SongController;
use \App\Http\Controllers\PlaylistController;
use \App\Http\Controllers\ImportController;
use \App\Http\Controllers\TS3Controller;

Route::get('/', function () {
    //return view('home');
    return view('about');
})->name('root');

DebugController::routes();
Auth::routes();
DbSettingController::routes();
UserController::routes();
GroupController::routes();

MusicBotController::routes();
MusicBotLockController::routes();
SongController::routes();
PlaylistController::routes();
ImportController::routes();
TS3Controller::routes();

Route::get('/about', function () {
    return view('about');
})->name('about');
