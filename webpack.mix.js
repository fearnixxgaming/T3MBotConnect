let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

require('dotenv').config();
mix.setResourceRoot(process.env.MIX_RESOURCE_ROOT);

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

mix.js('resources/assets/js/t3mbotconnect.js', 'public/js');

mix.scripts('node_modules/jquery-match-height/dist/jquery.matchHeight.js', 'public/js/jquery.matchHeight.js');

mix.scripts('node_modules/bootstrap-slider/dist/bootstrap-slider.js', 'public/js/bootstrap-slider.js')
    .styles('node_modules/bootstrap-slider/dist/css/bootstrap-slider.css', 'public/css/bootstrap-slider.min.css');