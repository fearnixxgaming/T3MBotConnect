<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (env('DB_ISMARIADB') == true) {
            Schema::defaultStringLength(191);
        }
        Schema::create('permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('permission')->unique();
            $table->smallInteger('category', false, true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permissions');
    }
}
