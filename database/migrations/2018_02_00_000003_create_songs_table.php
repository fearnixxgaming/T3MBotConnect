<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSongsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (env('DB_ISMARIADB') == true) {
            Schema::defaultStringLength(191);
        }
        Schema::create('songs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 220);
            $table->string('uri', 300);
            $table->string('external_id')->nullable();
            $table->integer('user_id', false, true);
        });

        Schema::table('songs', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->unique('external_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('songs');
    }
}
