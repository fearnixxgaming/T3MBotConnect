<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMusicBotLocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('musicbots_locks', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('created_on')->useCurrent();
            $table->timestamp('refreshed_on')->useCurrent();
            $table->integer('shared_via', false, true)->nullable();
            $table->integer('mbot_id', false, true);
            $table->integer('user_id', false, true);
        });

        Schema::table('musicbots_locks', function (Blueprint $table) {
            $table->foreign('shared_via')->references('id')->on('musicbots_locks')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('mbot_id')->references('id')->on('musicbots')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('musicbots_locks');
    }
}
