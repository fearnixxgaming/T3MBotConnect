<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMusicBotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (env('DB_ISMARIADB') == true) {
            Schema::defaultStringLength(191);
        }
        Schema::create('musicbots', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url');
            $table->smallInteger('port', false, true);
            $table->unique(['url', 'port']);
            $table->string('admin_pass');
            $table->string('display_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('musicbots');
    }
}
