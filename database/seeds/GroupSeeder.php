<?php
namespace App;

use Illuminate\Database\Seeder;

class GroupSeeder extends Seeder
{

    const DEFAULTS = [
        "user" => [
            "default" => true,
            "permissions" => [
                "mbot_view",
                "mbot_control"
            ]
        ],
        "admin" => [
            "default" => false,
            "permissions" => [
                "user_is_admin"
            ]
        ]
    ];

    private $permCache = [];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (GroupSeeder::DEFAULTS as $group => $data) {
            Group::updateOrCreate([
                "display_name" => $group,
                "default" => $data["default"]
            ]);
            $groupID = Group::whereDisplayName($group)->get()->get(0)->id;
            foreach ($data["permissions"] as $perm_name) {
                $permID = $this->getPermID($perm_name);
                PermissionLink::updateOrCreate([
                    "group_id" => $groupID,
                    "perm_id" => $permID
                ]);
            }
        }
    }

    private function getPermID(string $name) :int
    {
        if (array_key_exists($name, $this->permCache)) {
            return $this->permCache[$name];
        }
        $perms = Permission::wherePermission($name)->get();
        if ($perms->count() !== 1) {
            throw new \Exception("Permission not found: ".$name);
        }
        $perm = $perms->get(0);
        $this->permCache[$perm->permission] = $perm->id;
        return $perm->id;
    }
}
