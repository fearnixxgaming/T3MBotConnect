<?php
namespace App;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{

    const DEFAULT = [
        "admin" => [
            "email" => "admin@example.com",
            "password" => "admin",
            "name" => "DeleteMe",
            "active" => true,
            "banned" => false,
            "groups" => [
                "admin"
            ]
        ],
        "user" => [
            "email" => "user@example.com",
            "password" => "user",
            "name" => "TestUser",
            "active" => false,
            "banned" => false,
            "groups" => [
                "user"
            ]
        ],
        "user2" => [
            "email" => "user2@example.com",
            "password" => "user2",
            "name" => "TestUser2",
            "active" => false,
            "banned" => true,
            "groups" => [
                "user"
            ]
        ]
    ];

    private $groupIDCache = [];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (UserSeeder::DEFAULT as $username => $data) {
            $user = new User();
            $user->password = Hash::make($data['password']);
            $user->email = $data['email'];
            $user->name = $data['name'];
            $user->activated = $data['active'];
            $user->banned = $data['banned'];
            $user->save();

            $user_id = User::whereEmail($user->email)->get()->get(0)->id;

            foreach ($data['groups'] as $group) {
                GroupLink::updateOrCreate([
                    "user_id" => $user_id,
                    "group_id" => $this->getGroupID($group)
                ]);
            }
        }
    }

    private function getGroupID(string $groupName) :int
    {
        if (array_key_exists($groupName, $this->groupIDCache)) {
            return $this->groupIDCache[$groupName];
        }
        $groups = Group::whereDisplayName($groupName)->get();
        if ($groups->count() === 1) {
            $this->groupIDCache[$groupName] = $groups->get(0)->id;
            return $groups->get(0)->id;
        }
        throw new \Exception('Failed to find group: '.$groupName);
    }
}
