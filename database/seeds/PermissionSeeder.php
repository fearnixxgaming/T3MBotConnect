<?php
namespace App;

use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    const DEFAULTS = [
        Permission::BOTS_VIEW => ['cat' => 0],
        Permission::BOTS_CONTROL => ['cat' => 0],
        Permission::BOTS_EDIT => ['cat' => 2],
        Permission::SONG_VIEW => ['cat' => 0],
        Permission::SONG_REGISTER => ['cat' => 1],
        Permission::PLAYLIST_VIEW => ['cat' => 0],
        Permission::PLAYLIST_CREATE_PRIVATE => ['cat' => 1],
        Permission::PLAYLIST_CREATE_PUBLIC => ['cat' => 2],
        Permission::PLAYLIST_MODIFY_UNOWNED => ['cat' => 3],
        Permission::URI_IMPORT => ['cat' => 2],
        Permission::USERS_VIEW => ['cat' => 1],
        Permission::USERS_EDIT => ['cat' => 3],
        Permission::USERS_ASSIGN => ['cat' => 2],
        Permission::GROUPS_VIEW => ['cat' => 3],
        Permission::GROUPS_EDIT => ['cat' => 3],
        Permission::SETTINGS_EDIT => ['cat' => 4],
        Permission::ADMIN => ['cat' => 4]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (PermissionSeeder::DEFAULTS as $permission => $data) {
            Permission::updateOrCreate(
                [
                    'permission' => $permission,
                    'category' => $data['cat']
                ]
            );
        }
    }
}
