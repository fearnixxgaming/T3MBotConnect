<?php

use App\GroupSeeder;
use App\PermissionSeeder;
use App\UserSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SettingSeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(GroupSeeder::class);
        $this->call(UserSeeder::class);
    }
}
