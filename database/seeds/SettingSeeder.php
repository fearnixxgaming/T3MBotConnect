<?php

use App\DbSetting;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    const DEFAULTS = [
        'pagination_musicbots_pp' => '10',
        'pagination_songs_pp' => '20',
        'pagination_playlists_pp' => '20',
        'pagination_users_pp' => '50',
        'pagination_settings_pp' => '50',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (SettingSeeder::DEFAULTS as $setting => $defaultVal) {
            DbSetting::updateOrCreate(
                [
                    'ident' => $setting,
                    'value' => null,
                    'default' => $defaultVal
                ]
            );
        }
    }
}
